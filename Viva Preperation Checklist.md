# Viva Preparation

## General Definition

## Data Structure and Algorithms
- Arrays
- Linked List
- Stack
- Tree
- ArrayList class
- LinkedList class
- HashMap
- TreeMap
- TreeSet
- String manipulation

1. Big O notation [Complexity of Algorithms]
- Some common runtime
- Visualising difference using x y curve

2. Recursion
- Base case
- Recursive Case
- Stack Callback

3. Quick Sort / Merge Sort
- Divide and Conquer
- Merge vs Quick

4. Hash Table
- Basic Definition with visualisation
- Lookup
- Preventing Duplicate

5. BFS
- What is graph
- Finding shortest path
- Weighted Graph

6. Dijkstra's algorithm
- Terminology
- Nogative-weight edges
- Find firs-test path

7. Greedy algorithms
- How do you tell if a problem is NP-complete.
- Terminology
- Approximation algorithms
- NP problem
- Travelling salesperson

8. Dynamic Programming
- Basic Definition
- Knapsack Problem
- Subsequence

9. Binary Search
- Time complexity 
- Visualisation

## Object Oriented Programming [Java]

1. What is OOPS?
2. Write basic concepts of OOPS?
3. What is a class?
4. What is an object?
5. What is Encapsulation?
6. What is Polymorphism?
7. What is Inheritance?
8. What are manipulators?
9. Define a constructor?
10. Define Destructor?
11. What is Inline function?
12. What is operator overloading?
13. What is an abstract class?
14. What is an interface?
15. What is exception handling?
16. What is dynamic or run time polymorphism?
17. What is static and dynamic binding?
18. What is a copy constructor?
19. Difference between class and an object?
20. What is the difference between structure and a class?
21. What are different types of Inheritance supported by Java?
22. Why multiple Inheritance is not supported by Java? 
23. Why Inheritance is used by Java Programmers?
24. How to use Inheritance in Java?
25. What is the syntax of Inheritance?
26. What is the difference between Inheritance and Encapsulation? 
27. What is the difference between Inheritance and Abstraction?
28. What is the difference between Polymorphism and Inheritance?
29. What is the difference between Composition and Inheritance in OOP?
30. Can we override static method in Java?
31. Can we overload a static method in Java?
32. Can we override a private method in Java? 
33. What is method hiding in Java?
34. Can a class implement more than one interface in Java?
35. Can a class extends more than one class in Java?
36. Can an interface extends more than one interface in Java?
37. What will happen if a class extends two interfaces and they both have a method with same name and signature?
38. Can we pass an object of a subclass to a method expecting an object of the super class?
39. What is the Liskov substitution principle?
40. How to call a method of a subclass, if you are holding an object of the subclass in a reference variable of type superclass?

# OOPS Concept Interview Questions in Java

1. What is method overloading in OOP or Java? (answer)
2. What is method overriding in OOP or Java? (answer)
3. What is method hiding in Java? (answer)
4. Is Java a pure object oriented language? if not why? (answer)
5. What are rules of method overloading and overriding in Java? (answer)
6. The difference between method overloading and overriding? (answer)
7. Can we overload a static method in Java? (answer)
8. Can we override static method in Java? (answer)
9. Can we prevent overriding a method without using the final modifier? (answer)
10. Can we override a private method in Java? (answer)
11. What is covariant method overriding in Java? (answer)
12. Can we change the return type of method to subclass while overriding? (answer)
13. Can we change the argument list of an overriding method? (answer)
14. Can we override a method which throws runtime exception without throws clause? (answer)
15. How do you call superclass version of an overriding method in sub class? (answer)
16. Can we override a non-static method as static in Java? (answer)
17. Can we override the final method in Java? (answer)
18. Can we have a non-abstract method inside interface? (answer)
19. What is the default method of Java 8? (answer)
20. What is an abstract class in Java? (answer)
21. What is an interface in Java? What is the real user of an interface? (answer)
22. The difference between Abstract class and interface? (answer)

# Java Object Oriented Programming questions for experienced

1. Can we make a class abstract without an abstract method? (answer)
2. Can we make a class both final and abstract at the same time? (answer)
3. Can we overload or override the main method in Java? (answer)
4. What is the difference between Polymorphism, Overloading, and Overriding? (answer)
5. Can an interface extend more than one interface in Java?
6. Can a class extend more than one class in Java?
7. What is the difference between abstraction and polymorphism in Java? (answer)

# Advance Java Question 
1. Can we overload main in Java?
2. Can we override main in Java?
3. Can we make main final in Java?
4. Can we make main synchronized in Java?
5. How to call a nonstatic method from main in Java?
6. What is difference between Vector and ArrayList in Java?
7.  What is difference in LinkedList and ArrayList in Java?
8. What is difference between fail-fast and fail-safe Iterator in Java?
9. Difference between throw and throws in Java?
10. What is difference between checked and unchecked Exception in Java.
12. What happens if you don't call wait and notify from synchronized block?
11. What is difference between Runnable and Thread in Java ?
13. Write code to solve producer consumer problem in Java.
14. Difference between String, StringBuffer and StringBuilder in Java? (detailed answer)
15. Difference between extends Thread vs implements Runnable in Java? (detailed answer)
16. Difference between Runnable and Callable interface in Java? (detailed answer)
17. Difference between ArrayList and LinkedList in Java? (detailed answer)
18. What is difference between wait and notify in Java? (detailed answer)
19. Difference between HashMap and Hashtable in Java? (detailed answer)
20. Difference between TreeSet and TreeMap in Java? (detailed answer)
21. How to Swap two numbers without using temp variable? (solution)
22. Difference between Serializable and Externalizable in Java? (detailed answer)
23. Difference between transient and volatile in Java? (detailed answer)
24. Difference between abstract class and interface? (detailed answer)
25. Difference between Association, Composition and Aggregation? (detailed answer)
26. What is difference between FileInputStream and FileReader in Java? (detailed answer)
27. How do you convert bytes to character in Java? (detailed answer)
28. Can we have return statement in finally clause? What will happen? (detailed answer)
29. Can you override static method in Java? (detailed answer)
30. Difference between private, public, package and protected in Java? (detailed answer)
31. 5 Coding best practices you learned in Java? (detailed answer)
32. What is difference between calling start() and run() method of Thread? (detailed answer)
33. Write a Program to solve Producer Consumer problem in Java? (solution)
34. What is equlas() and hashCode() contract in Java? Where does it used? (detailed answer)
35. Why wait and notify methods are declared in Object class? (detailed answer)
36. How does HashSet works in Java? (detailed answer)37. What is difference between synchronize and concurrent Collection in Java? (detailed answer)
38. What is difference between Iterator and Enumeration in Java? (detailed answer)
39. What is difference between Overloading and Overriding in Java? (detailed answer)
40. Difference between static and dynamic binding in Java? (detailed answer)
41. Difference between Comparator and Comparable in Java? (detailed answer)
42. What is difference between PATH and CLASSPATH in Java? (detailed answer)
43. What is difference between Checked and Unchecked Exception in Java? (detailed answer)

# Java Collection Interview Questions Answers

1. What is Difference between Hashtable and HashMap in Java?
2. What is the difference between Hashtable and ConcurrentHashMap in Java?
3. What is Difference between Iterator and Enumeration in Java?
4. What is Difference between fail-safe and fail-fast Iterator in Java?
5. How HashMap works internally in Java?
6. Can you write code to traverse Map in Java on 4 ways?
7. What is the difference between ArrayList and LinkedList in Java?
8. What is the difference between List and Set in Java?
9. How do you find if ArrayList contains duplicates or not?
10. What is the difference between Vector and ArrayList in Java?

# JSP Servlet Interview Questions Answers

1. What is JSESSIONID in Java? When does JSESSIONID gets created ?
2. What is difference between include action and include directive in JSP?
3. How do you define application wide error page in JSP?
4. Difference between sendredirect and forward in Servlet ?
5. How do remove variable using <c:set> tag from JSTL ?
6. What is difference between Web Server and Application Server ?
7. What is difference between URL Encoding and URL rewriting ?
8. How do you get ServletContext reference inside Servlet ?
9. What is difference between ServletContext and ServletConfig in Java ?
10. Which open source tag library have you used ?
11. What is difference between GET and POST method in HTTP protocol?
12. What does load-on-start-up element in web.xml do?


Top 20 Essential Java Interview Questions with Answers for Freshers with 1 to 2 years Experienced

1. What's is Polymorphism in Java or OOP? (answer)
2. Given a reference to an abstract class Shape that has an abstract method named Draw, how does Java determine which version of the draw to call? What portion is handled at runtime and what portion at compile time?  (answer)
3. what is the difference between an array and a linked list? How does this affect performance and memory usage? (answer)
4. Describe garbage collection. What do mark and sweep mean? How about generational garbage collection? (answer)
5. what is the difference between a string literal and a string object? (answer)
6. what is the difference between int and Integer? (answer)
7. what is a binary search tree?  How do you implement in Java? (answer)
8. what is a heap memory in Java? (answer)
9. how would you implement a priority queue in Java? (answer)
10. what does "Big O of n" mean in practice? Will an O(n) algorithm always outperform an O(n*n) algorithm? (answer)
11. What is the difference between a list and a linked list in Java? (answer)
12. What is a map in Java? (answer)
13. what is an array in Java? (answer)
14. What is a thread in Java? (answer)
15. What is a lock in Java? (answer)
16. What is deadlock? (answer)
17. Difference between Overloading and Overriding? (answer)
18. Difference between abstract class and interface? (answer)
19. Difference between Process and Thread in Java? (answer)
20. Difference between "extends" thread and "implements" Runnable? (answer)

# 10 tough Java interview question and answer

1. Why wait and notify is declared in Object class instead of Thread ?
2. Why is multiple inheritances not supported in Java ?
3. Why does Java not support operator overloading ?
4. Why is String immutable in Java?
5. Why char array is preferred to store password than String in Java?
6. How to create a thread-safe singleton in Java using double-checked locking?
7. Write Java program to create a deadlock in Java and fix it ?
8. What happens if your Serializable class contains a member which is not  serializable? How do you fix it?
9. Why wait and notify  called from synchronized method in Java?
10. Can you override static method in Java? if I create the same method in the subclass is it compile time error?

- Polymorphism
- Inheritance
- Encapsulation
- Abstraction
+ Thread
	- Asynchronies 
	- Synchronise
	- Concurrent
	- Parallel

- Thread Implementation
- Lamda Expression
- Collection API
- Stream API
- SOLID Principle
- Design pattern basic[novice level]
- Low level implementation
	- Abstract/interface/
	- default / final
	- Garbage Collection
		- Memory Structure
		- Heap Memory
		- Non Heap Memory
		- Meta Space
		- Garbage Collector
			- Serial GC
			- Parallel GC
			- Garbage First Collector [G1]
			- Concurrent Mark and Sweep GC
		- Types of Garbage Collector
			- Concurrent Collector
			- Parallel Collector
			- Stop The world
		- Garbage Collector Steps
			- Marking
			- Sweep
			- Compact


## Operating System

- Threads and Concurrency
	- User Level Threads
	- Kernel Level Threads
- Interrupt
- Processes
	- Scheduling
	- Inter-Process Communication
- Memory Management
- Shared Memory Method
-  I/O Management
	- Special Instruction I/O
	- Memory-mapped I/O
- Direct memory access (DMA)
- Virtualisation
- Distributed File Systems
- Distributed Shared Memory


## Software Engineering [Agile/Scrum]

- Process Model
	- The waterfall model
	- Evolutionary Development model
	- Component-based Software Engineering
	- The Incremental Model
	- Prototyping
	- Rapid Application Development (RAD)
	- Boehm’s Spiral Model
	- The Rational Unified Process
	- Agile


## Computer Organisation and Architecture

## Database Design
- RDMS (Core Concept)
- Database Management System
- Relationship Concept 
- Designing A Database
- Characteristics of DB
- Database Schema
- Constraints
- ER MODEL
	- ENTITY
	- Attributes
	- ER MODEL
	- Composite Attributes
	- Multivalued Attribute
	- Derived Attribute 
	- Recursive Relationships
	- Cardinality Ratios for Binary Relationship
		- N:1 Relationship
		- M:N Relationship
		- Participation constraint
	- Normalisation
		+ First Normalisation
		+ Second Normalisation
		+ Third Normalisation
	- Functional Dependence
	-  Partial-key Dependency 
	- Transitive Dependency
	- RELATIONAL ALGEBRA
	- INDEXING AND B-TREE
	- RAID
	- Objective of normalisation

- SQL Query

## Backend
1. Spring Core
2. Spring MVC
3. Spring AOP
4. Spring Boot

## ORM
1. Hibernate

## Front End
1. jsp
2. Thyme-leaf

## Version Control
1. Git

# Sort Checklist given by mukit vaiya

1. Multithreading 
2. Producer-Consumer Problem  
3. Data Race  
4. Race Condition  
5. Synchronize     
6. Volatile    
7. Interface 
8. Abstract Class  
9. Use of Interface  
10. Benefit of Interface  
11. Example of polymorphism
12. Property of OOP    
13. Diamond Shape Problem 
14. Multiple Inheritance doesn't support but multiple interface support 
15. Dependency Inversion Problem  
16. SOLID Principle   
17. Singleton Pattern  
18. Factory Pattern  
19. Exception Handling   
20. Serialization   
21. Tightly Coupled 
22. Abstraction  
23. notify vs notifyAll 
24. Shallow Clonning vs Deep clonning

The 10 Operating System Concepts Software Developers Need to Remember
https://medium.com/cracking-the-data-science-interview/the-10-operating-system-concepts-software-developers-need-to-remember-480d0734d710

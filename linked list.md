# 01 Singly Linked List
01. Linked list vs array list 
02. Linked list insert node
03. Linked list insert node
04. Linked list deleting node
05. Delete a linked list node at given position
06. A programmers approach of looking at array vs linked list
07. Find length of a linked list(iterative and recursive)
08. Search an element in a linked list (iterative and recursive)
09. How to write c function that modify head pointer of a linked list
10. Swap nodes in a linked list without swapping data
11. Write a function to get nth node in a linked list
12. Find the middle of a given linked list in java
13. Program for nth node from the end of a linked list
14. Write a function to delete a linked list
15. Write a function that counts the number of times a given int occurs in a linked list
16. Merge two sorted linked lists
17. Generic linked list in c
18. Given a linked list which is sorted, how will you insert in sorted way
19. Given only a pointer/reference to a node to be deleted in a singly linked list , how do you delete it?
20. Function to check if a singly linked list is palindrome
21. Write a function to get the intersection point of two linked lists
22. Print reverse of a linked list without actually reversing
23. Remove duplicate from a sorted linked list
24. Remove duplicate from an unsorted linked list
25. Pairwise swap elements of a given linked list
26. Practice question for linked list and recursion
27. Move last element to front of a given linked list
28. Intersection of two sorted linked lists
29. Delete alternate nodes of a linked list
30. Alternations split of a given singly linked list | set 1
31. Identical linked lists
32. Merge sort for linked lists
33. Reverse a linked list in groups of given size | set 1
34. Reverse alternate k nodes in a singly linked list
35. Delete nodes which have a greater value on right side
36. Segregate even and odd nodes in a linked list
38. Detect and remove loop in a linked list
39. Add two numbers represented by linked lists | set 1
40. Delete a given node in linked list under given constraints
41. Union and intersection of two linked lists
42. Find a triplet from three linked list with sum equal to a given number
43. Rotate a linked list
44. Flattening a linked list
45. Add two numbers represented by linked lists | set 2
46. Sort a linked list of 0s, 1s and 2s
47. Flatten a multilevel linked list
48. Delete n nodes after m nodes of linked list
49. Quick sort on singly linked list
50. Merge a linked list int another linked list at alternate positions
51. Pairwise swap elements of a given linked list by changing links
52. Given a linked list of line segments, remove middle points
53. Construct a maximum sum linked list out of two sorted linked lists having some common nodes
54. Can we reverse a linked list in less then O(n)?
55. Clone a linked list with next and random pointer | set 1
56. Clone a linked list with next and random pointer | set 2
57. Insertion sort for singly linked list
58. Point to next higher value node in a linked list with an arbitrary pointer
59. Rearrange a given linked list in-place
60. Sort a linked list that is sorted alternation ascending and descending orders?
61. Select a random node from a singly linked list
62. Why quick sort preferred for arrays and merge sort for linked list?
63. Merge two sorted linked list such that merged list is in reverse order
64. Compare two string represented as linked lists
65. Rearrange a linked list such that all even and odd positioned nodes are together
66. Rearrange a linked list in zig-zag fashion
67. Add 1 to a number represented as linked list
68. Point orbit pointer to greatest value right side node in a linked list
69. Convert a given binary tree to doubly linked list | set 4
70. Check if a linked list of string forms a palindrome
71. Sort linked list which is already sorted on absolute values
72. Delete last occurrence of an item from linked list
73. Linked list in java


# 02 Circular Linked List


01. Circular linked list | set 1
02. Circular linked list | set 2(traversal)
03. Split circular linked list int two halves
04. Sorted insert for circular linked list
05. Sorted insert for circular linked list

# 03 Doubly Linked List

01. Doubly linked list | set 1
02. Delete a node in a doubly linked list
03. Reverse a doubly linked list
04. The great tree-list recursion problem
05. Clone a linked list with next and random pointer | set 1
06. Quick short on Doubly linked list
07. Swap nth node from beginning with nth node from end in a linked list
08. Merge sort for doubly linked list

total 86
Next graph
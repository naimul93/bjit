package com.ignite.abdullah.array;

import java.util.Arrays;

public class _05Anagram {
	public static void main(String[] args) {
		String s1 = "AAB";
		String s2 = "ABA";

		char[] c1 = s1.toCharArray();
		char[] c2 = s2.toCharArray();

		Arrays.sort(c1);
		Arrays.sort(c2);
		boolean flag = false;
		if (c1.length == c2.length) {
			for (int i = 0; i < c2.length; i++) {
				if (c1[i] == c2[i])
					flag = true;
				else {
					flag = false;
					break;
				}
			}
		}
		System.out.println("Anagram status : " + flag);
	}
}

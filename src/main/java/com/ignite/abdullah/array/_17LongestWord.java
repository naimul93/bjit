package com.ignite.abdullah.array;
import static com.ignite.StringUtil.print.Print.print ;
public class _17LongestWord {
	public static void main(String[] args) {

		var str = "I love dogs";

		var arr = str.split(" ");
		var res = "";
		for (var i : arr) {
			if (res.length() < i.length())
				res = i;
		}
		print(res);
	}

}

package com.ignite.abdullah.array;

import java.util.ArrayList;

public class _18ClosestEnemyII {
	public static String ClosestEnemyII(String[] strArr) {

		int[] start = new int[] { 0, 0 };
		int closest = 0;
		int x;
		int y;
		int temp0, temp1; // for clarity
		ArrayList<int[]> enemies = new ArrayList<>();
		char[][] matrix = new char[strArr.length][];
		for (int i = 0; i < strArr.length; i++) {
			matrix[i] = strArr[i].toCharArray();
		}
		
		
		for (char[] cs : matrix) {
			for (char cs2 : cs) {
				System.out.print(cs2 + " ");
			}
			System.out.println();
		}
		
		
		for (int i = 0; i < matrix.length; i++) { // enumeration
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == '1') {
					start = new int[] { i, j };
				}
				if (matrix[i][j] == '2') {
					enemies.add(new int[] { i, j });
				}
			}
		}
		for (int[] e : enemies) {
			temp0 = Math.abs(strArr[0].length() - start[0] + e[0]);
			temp1 = Math.abs(strArr[0].length() - e[0] + start[0]);
			x = Math.min(Math.abs(start[0] - e[0]), Math.min(temp0, temp1));
			temp0 = Math.abs(strArr.length - start[1] + e[1]);
			temp1 = Math.abs(strArr.length - e[1] + start[1]);
			y = Math.min(Math.abs(start[1] - e[1]), Math.min(temp0, temp1));

			if (closest == 0 || closest > (x + y)) {
				closest = x + y;
			}
		}
		return String.valueOf(closest);
	}

	public static void main(String[] args) {
		System.out.print(ClosestEnemyII(new String[] { "000", "100", "200" }));
	}
}

package com.ignite.abdullah.array;

public class _13FindTheMissingNumber {

	public static void main(String[] args) {
		var arr = new int[] { 1, 2, 4, 6, 3, 7, 8 };
		var l = arr.length;
		var formula = ((l+1)*(l+2))/2;
		
		for (var i = 0; i < l; i++) {
			formula -= arr[i];
		}
		
		System.out.println(formula);
	}
}

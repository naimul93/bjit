package com.ignite.abdullah.array;

public class _20RemoveDuplicatesFromSortedArray {
	private static int removeDuplicate(int[] arr) {

		int[] temp = new int[arr.length];
		int i = 0;
		for (int j = 0; j < arr.length - 1; j++) {
			temp[i] = arr[j];
			i++;
			while ((arr[j] == arr[j + 1]) && j < arr.length - 2) {
				j++;
			}
		}
		
		if(arr[arr.length-2] != arr[arr.length-1]) 
			temp[i++] = arr[arr.length-1];
		
		for (int j = 0; j < i; j++) {
			arr[j] = temp[j];
		}
		return i;
	}

	public static void main(String[] args) {
		int[] arr = { 1, 2, 2, 3, 4, 4, 4, 5, 5,6 };

		int newLength = removeDuplicate(arr);

		for (int i = 0; i < newLength; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}

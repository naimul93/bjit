package com.ignite.abdullah.array;

import java.util.LinkedHashMap;
import java.util.Map;

public class _09LRUCache {
	LinkedHashMap<Integer, Integer> map = new LinkedHashMap<> (); 
	@SuppressWarnings("serial")
	public _09LRUCache(int capacity) {
        map = new LinkedHashMap<>(capacity , 0.75f , true){
		
			//removing the eldest from map
            @SuppressWarnings("rawtypes")
			protected boolean removeEldestEntry(Map.Entry eldest){
              return map.size()>capacity;   
            }
        };
    }

	public int get(int key) {
		return map.getOrDefault(key, -1);
	}

	public void put(int key, int value) {
		map.put(key, value);
	}

	public static void main(String[] args) {
		_09LRUCache cache = new _09LRUCache( 2 /* capacity */ );

		cache.put(1, 1);
		cache.put(2, 2);
		cache.get(1);       // returns 1
		cache.put(3, 3);    // evicts key 2
		cache.get(2);       // returns -1 (not found)
		cache.put(4, 4);    // evicts key 1
		cache.get(1);       // returns -1 (not found)
		cache.get(3);       // returns 3
		System.out.println(cache.get(4));       // returns 4
	}
}

package com.ignite.abdullah.array;

public class _04Palindrome {
	static boolean flag = false;

	public static void main(String[] args) {
		String arr = "ABCDDCBA";
		char[] arr2 = arr.toCharArray();
		recurs(arr2, 0, arr2.length - 1);
	}

	private static void recurs(char[] arr2, int start, int end) {
		System.out.println("Start : " + start + " End : " + end);
		if (start <= end) {
			if (start == end) {
				System.out.println(flag);
			} else if (start == end - 1) {
				System.out.println(flag);
			} else if (arr2[start] == arr2[end]) {
				flag = true;
				recurs(arr2, ++start, --end);
			} else {
				flag = false;
				System.out.println(flag);
				return;
			}
		}
	}
}

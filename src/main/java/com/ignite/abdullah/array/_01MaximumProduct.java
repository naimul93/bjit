package com.ignite.abdullah.array;

public class _01MaximumProduct {
	public static void main(String[] args) {
		int[] arr = { 5, 3, 6, 9, 8 };

		int max = 1;

		for (int i = 0; i < arr.length; i++) {

			for (int j = i + 1; j < arr.length; j++) {
				int temp = arr[i] * arr[j];

				if (max < temp)
					max = temp;
			}
		}
		System.out.println("max value is : " + max);
	}
}

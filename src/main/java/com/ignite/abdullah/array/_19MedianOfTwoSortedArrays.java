package com.ignite.abdullah.array;

public class _19MedianOfTwoSortedArrays {

	static int maximum(int a, int b) {
		return a > b ? a : b;
	}

	static int minimum(int a, int b) {
		return a < b ? a : b;
	}

	static double findMedianSortedArrays(int[] arr1, int[] arr2) {

		int n = arr1.length, m = arr2.length;
		int min = 0, max = n;
		int i = 0, j = 0, median = 0;

		while (min <= max) {
			i = (min + max) / 2;
			j = ((n + m + 1) / 2) - i;
			if (i < n && j > 0 && arr2[j - 1] > arr1[i])
				min = i + 1;
			else if (i > 0 && j < m && arr2[j] < arr1[i - 1])
				max = i - 1;
			else {
				if (i == 0)
					median = arr2[j - 1];
				else if (j == 0)
					median = arr1[i - 1];
				else
					median = maximum(arr1[i - 1], arr2[j - 1]);
				break;
			}
		}

		if ((n + m) % 2 == 1)
			return (double) median;

		if (i == n)
			return (median + arr2[j]) / 2.0;

		if (j == m)
			return (median + arr1[i]) / 2.0;

		return (median + minimum(arr1[i], arr2[j])) / 2.0;
	}

	public static void main(String[] args) {
		int[] arr1 = new int[] { 900 };
		int[] arr2 = new int[] { 10, 13, 14 };
		System.out.print("The median is : " + findMedianSortedArrays(arr1, arr2));
	}
}
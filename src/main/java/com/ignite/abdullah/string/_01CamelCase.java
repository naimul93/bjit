package com.ignite.abdullah.string;

import java.io.IOException;
//https://www.hackerrank.com/challenges/camelcase/problem
public class _01CamelCase {

	// Complete the camelcase function below.
	static int camelcase(String s) {
		char[] ch = s.toCharArray();
		int count = 1;
		for (int i = 0; i < ch.length; i++)
			if (ch[i] >= 'A' && ch[i] <= 'Z')
				count++;

		return count;
	}

//	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//		String s = scanner.nextLine();

		System.out.println(camelcase("oneTwoThree"));

//		bufferedWriter.write(String.valueOf(result));
//		bufferedWriter.newLine();
//		bufferedWriter.close();
//		scanner.close();
	}

}

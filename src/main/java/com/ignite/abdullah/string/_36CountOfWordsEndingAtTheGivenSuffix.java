package com.ignite.abdullah.string;

public class _36CountOfWordsEndingAtTheGivenSuffix {

	static int endingWith(String str, String suff) {
		int cnt = 0;
		String words[] = str.split(" ");
		for (int i = 0; i < words.length; i++)
			if (words[i].endsWith(suff))
				cnt++;

		return cnt;
	}

	public static void main(String args[]) {
		String str = "GeeksForGeeks is a computer science portal for geeks";
		String suff = "ks";

		System.out.print(endingWith(str, suff));
	}

}

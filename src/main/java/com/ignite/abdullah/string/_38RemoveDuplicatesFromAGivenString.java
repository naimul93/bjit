package com.ignite.abdullah.string;

import java.util.HashSet;
import java.util.Set;

public class _38RemoveDuplicatesFromAGivenString {

	static void removeDuplicates(String str) {
		Set<Character> lhs = new HashSet<>();
		for (int i = 0; i < str.length(); i++)
			lhs.add(str.charAt(i));

		for (Character ch : lhs)
			System.out.print(ch);
	}

	public static void main(String args[]) {
		String str = "cmabdullah";
		removeDuplicates(str);
	}
}
package com.ignite.abdullah.string;

public class _27MorganAndAString {

	static String morganAndString(String a, String b) {
		var result = new StringBuilder(a.length() + b.length());
		var i = 0;
		var j = 0;

		while (i < a.length() && j < b.length()) {
			if (breakTie(a, b, i, j) == 1) {
				var bb = b.charAt(j);
				while (j < b.length() && bb == b.charAt(j)) {
					result.append(bb);
					j++;
				}
			} else {
				var aa = a.charAt(i);
				while (i < a.length() && aa == a.charAt(i)) {
					result.append(aa);
					i++;
				}
			}
		}

		result.append(a.substring(i));
		result.append(b.substring(j));

		return result.toString();

	}

	static int breakTie(String a, String b, int i, int j) {
		while (i < a.length() && j < b.length()) {
			if (a.charAt(i) < b.charAt(j))
				return -1;
			if (a.charAt(i) > b.charAt(j))
				return 1;
			i++;
			j++;
		}
		return i < a.length() ? -1 : 1;
	}

	public static void main(String[] args) {
		System.out.println(morganAndString("JACK", "DANIEL"));
	}

}

package com.ignite.abdullah.string;

public class _11Gemstones {

	static int gemstones(String[] arr) {
		int n = arr.length;
		char ch = 'a';
		int[] a = new int[26];
		int count = 0;
		for (int i = 0; i < n; i++) {
			String s = arr[i];
			for (int j = 0; j < s.length(); j++)
				if (a[s.charAt(j) % ch] == i)
					a[s.charAt(j) % ch]++;
		}
		for (int i = 0; i < 26; i++)
			if (a[i] == n)
				count++;
		return count;
	}

	public static void main(String[] args) {
		System.out.println(gemstones(new String[] { "abcdde", "baccd", "eeabg" }));
	}
}

package com.ignite.abdullah.string;

public class _13BeautifulBinaryString {
	// Complete the beautifulBinaryString function below.
	static int beautifulBinaryString(String b) {
		int count = 0;
		char[] arr = b.toCharArray();
		for (int i = 2; i < b.length(); i++) {
			if (arr[i] == '0' && arr[i - 2] == '0' && arr[i - 1] == '1') {
				count++;
				i += 2;
			}
		}
		return count;

	}

	public static void main(String[] args) {
		System.out.println(beautifulBinaryString("0100101010"));
	}
}

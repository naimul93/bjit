package com.ignite.abdullah.string;

public class _34QueriesToPrintTheCharacterThatOccursTheMaximumNumberOfTimesInAGivenRange {

	static void solveQueries(String str, int start, int end) {

		String temp = str.substring(start, end + 1);

		int[] arr = new int[256];

		for (int i = 0; i < temp.length(); i++) {
			arr[(temp.charAt(i))]++;
		}

		int max = 0;
		int res = 0;
		for (int i = 0; i < arr.length; i++) {

			if (arr[i] > max) {
				max = arr[i];
				res = i;
			}
		}

		System.out.println((char)(res));

	}

	public static void main(String[] args) {
		String str = "striver";

//	    query.push_back({ 0, 1 }); 
//	    query.push_back({ 1, 6 }); 
//	    query.push_back({ 5, 6 }); 

		solveQueries(str, 1, 6);
	}
}

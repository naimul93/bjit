package com.ignite.abdullah.string;

public class _03TwoCharactersBrutForce {

	static int countAlternating(String s, char c, char d) {
		int n = 0;
		boolean last_c = false;
		boolean last_d = false;

		for (char a : s.toCharArray()) {
			if (a == c) {
				if (last_c)
					return 0;
				n++;
				last_c = true;
				last_d = false;
			}
			if (a == d) {
				if (last_d)
					return 0;
				n++;
				last_c = false;
				last_d = true;
			}
		}
		return n;
	}

	public static void main(String[] args) {

		String str = "beabeefeab";
		int m = 0;
		if (str.length() > 1) {
			for (char i = 'a'; i <= 'z'; i++) {
				for (char j = 'a'; j <= 'z'; j++) {
					if (i == j)
						continue;
					int n = countAlternating(str, i, j);
					if (n > m)
						m = n;
				}
			}
		}
		System.out.println(m);

	}
}

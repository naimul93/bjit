package com.ignite.abdullah.string;
//https://www.geeksforgeeks.org/caesar-cipher/
public class _04CaesarCipher {
	
	
	public static void main(String[] args) {
        String word = "ATTACKATONCE";
        int n = word.length();
        int k = 4;
        
        StringBuilder sb = new StringBuilder();
        
        for(int i = 0; i < n; ++i) {
            sb.append(encryptChar(word.charAt(i), k));
        }
        
        System.out.println(sb.toString());
    }
    
    private static char encryptChar(char c, int k) {
        if(!Character.isAlphabetic(c)) return c;
        int base = Character.isLowerCase(c) ? 'a' : 'A';
        return (char)((c + k - base) % 26 + base);
    }
}

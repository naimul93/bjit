package com.ignite.abdullah.string;

public class _31MaximumLengthPrefixOfOneStringThatOccursAsSubsequenceInAnother {
	static int maxPrefix(String s, String t) {
		int count = 0;
		for (int i = 0; i < t.length(); i++) {
			if (count == t.length())
				break;
			if (t.charAt(i) == s.charAt(count))
				count++;
		}
		return count;
	}

	public static void main(String args[]) {
		String S = "digger";
		String T = "biggerdiagram";
		System.out.println(maxPrefix(S, T));
	}
}

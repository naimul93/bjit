package com.ignite.abdullah.string;

public class _09SeparateTheNumbers {
	static void separateNumbers(String s) {

		var str = s;
		var numLen = str.length();
		var firstNum = 0;
		var incre = 0;
		var testStr = "";

		if (numLen <= 1) {
			System.out.println("NO");
			return ;

		}
		for (var i = 1; i <= numLen / 2; i++) {
			firstNum = Integer.parseInt(str.substring(0, i));
			testStr = String.valueOf(firstNum);
			incre = firstNum;

			while (testStr.length() < numLen && !(str.equals(testStr))) {
				incre++;
				testStr += String.valueOf(incre);
			}

			if (testStr.equals(str))
				break;

		}

		String res = testStr.equals(str) ? "YES " + firstNum : "NO";
		System.out.println(res);

	}

	public static void main(String[] args) {
		separateNumbers("1234");
	}
}

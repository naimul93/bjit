package com.ignite.abdullah.string;

public class _03TwoCharacters {

	static int alternate(String str) {
		int NO = 26;
		int length = str.length();
		if (length <= 1) {
			return 0;
		}
		/* Create arrays representing the 26^2 subproblems */
		int[][] pair = new int[NO][NO];
		int[][] count = new int[NO][NO];

		for (int i = 0; i < length; i++) {
			char letter = str.charAt(i);
			int letterNum = letter - 'a';

			for (int col = 0; col < NO; col++) {
				if (pair[letterNum][col] == letter) {
					count[letterNum][col] = -1;
				}
				if (count[letterNum][col] != -1) {
					pair[letterNum][col] = letter;
					count[letterNum][col]++;
				}
			}

			for (int row = 0; row < NO; row++) {
				if (pair[row][letterNum] == letter) {
					count[row][letterNum] = -1;
				}
				if (count[row][letterNum] != -1) {
					pair[row][letterNum] = letter;
					count[row][letterNum]++;
				}
			}
		}
		
		
		for (int row = 0; row < NO; row++) {
			for (int col = 0; col < NO; col++) {
				System.out.print(count[row][col] + " ");
			}
			System.out.println();
		}
		

		int max = 0;
		for (int row = 0; row < NO; row++) {
			for (int col = 0; col < NO; col++) {
				max = Math.max(max, count[row][col]);
			}
		}
		return max;

	}

	public static void main(String[] args) {
		System.out.println(alternate("beabeefeab"));
	}

}

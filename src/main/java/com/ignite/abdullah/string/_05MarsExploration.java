package com.ignite.abdullah.string;

public class _05MarsExploration {
	static int marsExploration(String s) {

		String sos = "SOS";
		String message = s;
		int count = 0;
		for (int i = 0; i < message.length(); i++) {
			if (message.charAt(i) != sos.charAt(i % 3))
				count++;
		}
		return count;
	}

	public static void main(String[] args) {
		System.out.println(marsExploration("SOSSOS "));
	}
}

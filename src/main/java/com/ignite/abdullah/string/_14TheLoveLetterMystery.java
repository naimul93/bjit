package com.ignite.abdullah.string;

public class _14TheLoveLetterMystery {

	// Complete the theLoveLetterMystery function below.
	static int theLoveLetterMystery(String s) {
		int oper = 0, n = s.length();
		for (int i = 0, j = n - 1; i < n / 2; i++, j--)
			oper += Math.abs(s.charAt(i) - s.charAt(j));
		return oper;
	}

	public static void main(String[] args) {

		System.out.println(theLoveLetterMystery("abcd"));
	}
}

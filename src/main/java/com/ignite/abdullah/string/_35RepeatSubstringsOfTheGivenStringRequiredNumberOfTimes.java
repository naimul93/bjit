package com.ignite.abdullah.string;

public class _35RepeatSubstringsOfTheGivenStringRequiredNumberOfTimes {


	static int nextNonDigit(String str, int i) {
		while (i < str.length() && Character.isDigit(str.charAt(i))) {
			i++;
		}

		if (i >= str.length())
			return -1;
		return i;
	}

	static void appendRepeated(StringBuilder sb, String str, int times) {
		for (int i = 0; i < times; i++)
			sb.append(str);
	}
	static String findString(String str, int n) {
		StringBuilder sb = new StringBuilder("");
		int startStr = nextNonDigit(str, 0);
		while (startStr != -1) {
			int endStr = startStr;
			while ((endStr + 1) < n && !Character.isDigit(str.charAt(endStr + 1))) {
				endStr++;
			}
			int startNum = endStr + 1;
			if (startNum == -1)
				break;
			int endNum = startNum;
			while ((endNum + 1) < n && Character.isDigit(str.charAt(endNum + 1)))  {
				endNum++;
			}
			int num = Integer.parseInt(str.substring(startNum, endNum + 1));
			appendRepeated(sb, str.substring(startStr, endStr + 1), num);
			startStr = nextNonDigit(str, endStr + 1);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String str = "g1ee1ks1for1g1e2ks1";
		int n = str.length();
		System.out.println(findString(str, n));
	}
}

package com.ignite.abdullah.string;

public class _39HowCanAGivenStringBeReversedUsingRecursion {

	public static void main(String[] args) {
		reverse("abdullah");
	}

	private static void reverse(String string) {
		
		if (string == null || string.isEmpty())
			return ;
		System.out.print(string.charAt(string.length()-1));
		reverse(string.substring(0, string.length()-1));
		
	}

}

package com.ignite.abdullah.string;

public class _02StrongPassword {

	static int minimumNumber(int n, String password) {
		int d = 0, l = 0, u = 0, s = 0;
		int length = password.length();
		for (int i = 0; i < length; i++) {
			char ch = password.charAt(i);
			if ('a' <= ch && ch <= 'z')
				l = 1;
			else if ('A' <= ch && ch <= 'Z')
				u = 1;
			else if ('0' <= ch && ch <= '9')
				d = 1;
			else
				s = 1;
		}

		int sum = d + l + u + s;
		if (sum == 4 && length >= 6)
			return 0;

		if (length < 6) {
			int missing = 4 - sum;
			if ((missing > 2) && (missing < length))
				return missing;
			else if (missing == 2 && length == 3)
				return length;
			else if (missing == 2 & length == 5)
				return missing;
			else
				return 6 - n;

		}

		return (4 - sum);

	}

	public static void main(String[] args) {
		System.out.println(minimumNumber(3, "Ab1"));
	}

}

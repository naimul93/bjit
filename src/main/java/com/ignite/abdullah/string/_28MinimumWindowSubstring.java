package com.ignite.abdullah.string;

public class _28MinimumWindowSubstring {
	public static String minWindow(String s, String t) {
		if (s.isEmpty())
			return "";
		int[] freq = new int[256];
		for (int i = 0; i < t.length(); i++)
			freq[t.charAt(i)]++;

		char[] arr = s.toCharArray();
		int l = 0, r = 0, missing = t.length(), i = 0, j = 0;
		while (r < s.length()) {
			if (freq[arr[r]] > 0)
				missing--;
			freq[arr[r]]--;
			r++;
			while (missing == 0) {
				if (j == 0 || (r - l) < (j - i)) {
					j = r;
					i = l;
				}
				freq[arr[l]]++;
				if (freq[arr[l]] > 0)
					missing++;
				l++;
			}
		}
		return s.substring(i, j);
	}

	public static void main(String[] args) {
		System.out.println(minWindow("a test string", "tist"));
	}
}
/****
Smallest window is :
t stri
*/
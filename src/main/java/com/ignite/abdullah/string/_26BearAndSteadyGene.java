package com.ignite.abdullah.string;

import java.util.HashMap;
import java.util.Map;

public class _26BearAndSteadyGene {
	static int steadyGene(String gene) {

		String s = gene;
		int n = s.length(), left = 0, right = 0, min = Integer.MAX_VALUE;
		Map<Character, Integer> map = new HashMap<>();
		map.put('A', 0);
		map.put('C', 0);
		map.put('G', 0);
		map.put('T', 0);
		
		
		for (char c : s.toCharArray())
			map.put(c, map.get(c) + 1);

		while (right < n - 1) {
			char rc = s.charAt(right++);
			map.put(rc, map.get(rc) - 1);
			while (isSteady(map, n)) {
				min = Math.min(min, right - left);
				char lc = s.charAt(left++);
				map.put(lc, map.get(lc) + 1);
			}
		}
		return min;

	}

	private static boolean isSteady(Map<Character, Integer> map, int n) {
		for (Integer i : map.values())
			if (i > n / 4)
				return false;
		return true;
	}

	public static void main(String[] args) {
		System.out.println(steadyGene("GAAATAAA"));
	}
}

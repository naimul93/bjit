package com.ignite.abdullah;

public class _01DuplicateNode {
	public static Node head;

	static class Node {

		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	static void removeDuplicates() {

		if (head == null || head.next == null) {
			return;
		}
		Node current = head;

		while (current != null && current.next != null) {
			if (current.data == current.next.data) {
				current.next = current.next.next;
			} else {
				current = current.next;
			}
		}
	}

	static void display(Node head) {
		Node current = head;
		while (current != null) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.println(current);
	}

	public static void main(String[] args) {

		head = new Node(10);
		head.next = new Node(11);
		head.next.next = new Node(12);
		head.next.next.next = new Node(12);
		head.next.next.next.next = new Node(12);
		head.next.next.next.next.next = new Node(13);
		head.next.next.next.next.next.next = new Node(13);

		display(head);

		removeDuplicates();

		display(head);
	}
}

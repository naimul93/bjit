package com.ignite.abdullah;

import java.util.Arrays;

public class _05Array {

	public static void main(String[] args) {
		int[] arr = { 2, 3, 4, 6, 9, 7, 8 };
		int count = 0;
		Arrays.sort(arr);
		
//		for (int i : arr) {
//			System.out.print(i + " ");
//		}
		
		for (int i = 2 ; i< arr.length ; i++) {
			
			int a = arr[i-2]+2;
			int b = arr[i-1]+1;
			int c = arr[i];
			
			if (a == b && b == c)
				count++;
		}
		
		System.out.println(count);
		
	}

}

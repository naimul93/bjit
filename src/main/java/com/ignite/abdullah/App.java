package com.ignite.abdullah;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class App {
	public static void main(String[] args) {
		
		//string
		System.out.println("string");
		ArrayList<String> listS = new ArrayList<>();
		listS.add("Abdullah");
		listS.add("Naimul");
		listS.forEach(n -> System.out.println(n));
		
		//object
		System.out.println("object");
		ArrayList<Person> list = new ArrayList<>();
		list.add(new Person("Abdullah", "cm@gmail.com", "0171"));
		list.add(new Person("Naimul", "naim@gmail.com", "0191"));
		list.forEach(n -> System.out.println(n));
		
		//string
		System.out.println("string");
		Map<String, String> map = new HashMap<>();
		map.put("abdullah", "khan");
		map.put("Naimul", "haque");
		
		for (Entry<String, String> it : map.entrySet()) {
			System.out.println(it.getValue());
		}
		
		//map object
		System.out.println("map object");
		Map<String, Person> mapO = new HashMap<>();
		mapO.put("abdullah", new Person("Abdullah", "cm@gmail.com", "0171"));
		mapO.put("Naimul", new Person("Naimul", "naim@gmail.com", "0191"));
		
		for (Entry<String, Person> it : mapO.entrySet()) {
			System.out.println(it.getValue().toString());
		}
		
		//map list
		System.out.println("map list");
		Map<String, ArrayList<Person>> mapList = new HashMap<>();
		mapList.put("abdullah", list);
		
		for (Entry<String, ArrayList<Person>> it : mapList.entrySet()) {
			it.getValue().forEach(n -> System.out.println(n));
		}
	}
}

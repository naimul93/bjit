package com.ignite.abdullah;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

public class _04StqckAndQueue {

	public static void main(String[] args) {
		String name = "cmamc";
		System.out.println(checkPalindrome(name));
	}

	static boolean checkPalindrome(String name) {
		Stack<Character> st = new Stack<>();
		Queue<Character> queue = new ArrayBlockingQueue<>(name.length());

		for (int i = 0; i < name.length(); i++)
			st.push(name.charAt(i));

		for (int i = 0; i < name.length(); i++)
			queue.add(name.charAt(i));

		for (int i = 0; i < name.length(); i++)
			if (st.pop() != queue.poll())
				return false;

		return true;
	}
}

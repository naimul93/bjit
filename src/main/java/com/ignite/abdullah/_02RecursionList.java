package com.ignite.abdullah;

import java.util.Stack;

public class _02RecursionList {

	public static void main(String[] args) {

		int n = 123;
		Stack<Integer> st = new Stack<>();
		Stack<Integer> res = recurse(n, st);
		
//		System.out.println(res.get(0));
		

		
		System.out.print(res.pop() + " ");
		System.out.print(res.pop() + " ");
		System.out.print(res.pop() + " ");

	}

	static Stack<Integer> recurse(int n, Stack<Integer> st) {

		if (n == 0)
			return st;

		int mod = n % 10;

		st.push(mod);
		n = n / 10;
		return recurse(n, st);
	}
}

package com.ignite.abdullah;
//You have to add the numbers as the same type,
public class Calculator<T extends Number> {
	private T a;
	private T b;
	private Object c;

	public Calculator(T a, T b) {
		this.a = a;
		this.b = b;
		this.c = new Object();
	}

	@SuppressWarnings("unchecked")
	public T add() {
		if (a instanceof Double)
			c = (a.doubleValue() + b.doubleValue());
		else if (a instanceof Integer)
			c = (a.intValue() + b.intValue());
		else if (a instanceof Float)
			c = (a.floatValue() + b.floatValue());
		return (T)c;
	}

	public static void main(String[] args) {
		Calculator<Integer> obj = new Calculator<Integer>(3, 2);
		System.out.println(obj.add());
	}
}
package com.ignite.abdullah.object.analysis;

public class Cow extends Animal{

	private Eye eye;
	private Nose nose;
	
	public Eye getEye() {
		return eye;
	}
	public void setEye(Eye eye) {
		this.eye = eye;
	}
	public Nose getNose() {
		return nose;
	}
	public void setNose(Nose nose) {
		this.nose = nose;
	}
	@Override
	public String toString() {
		return "Cow [eye=" + eye + ", nose=" + nose + "]";
	}
	
}

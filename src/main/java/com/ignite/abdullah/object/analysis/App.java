package com.ignite.abdullah.object.analysis;

public class App {

	
	public static void main(String[] args) {
		
		Eye eye = new Eye();
		eye.setColor("Blue");
		
		Nose nose = new Nose();
		nose.setType("Fede");
		
		Cow cow = new Cow();
		cow.setEye(eye);
		cow.setNose(nose);
		cow.setAnimalName("Cow");
		
		cow.eat();
		cow.run();
//		System.out.println(cow.toString());
		
		
	}
}

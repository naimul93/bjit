package com.ignite.abdullah.object.analysis;

public class Animal {
	
	private String animalName;
	
	public String getAnimalName() {
		return animalName;
	}
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}
	public void  eat() {
		System.out.println(animalName + " is eating...");
	}
	public void  run() {
		System.out.println(animalName+ " is running...");
	}
	@Override
	public String toString() {
		return "Animal [animalName=" + animalName + "]";
	}
	
	

}

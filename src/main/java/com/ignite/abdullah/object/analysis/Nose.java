package com.ignite.abdullah.object.analysis;

public class Nose {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Nose [type=" + type + "]";
	}
	
	
}

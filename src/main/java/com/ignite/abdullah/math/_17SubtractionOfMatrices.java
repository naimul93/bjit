package com.ignite.abdullah.math;

public class _17SubtractionOfMatrices {

	static final int N = 3;

	static void multiply(int A[][], int B[][], int C[][]) {
		int i, j;
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
				C[i][j] = A[i][j] - B[i][j];
	}

	public static void main(String[] args) {
		int[][] A = { 
				{ 1, 1, 1 }, 
				{ 2, 4, 2 }, 
				{ 3, 3, 3 } 
				};

		int[][] B = {
				{ 2, 1, 1 }, 
				{ 2, 2, 2 }, 
				{ 3, 3, 3 } 
				};

		int[][] C = new int[N][N];

		int i, j;
		multiply(A, B, C);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++)
				System.out.print(C[i][j] + " ");
			System.out.print("\n");
		}
	}

}

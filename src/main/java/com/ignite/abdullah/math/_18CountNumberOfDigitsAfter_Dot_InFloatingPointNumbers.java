package com.ignite.abdullah.math;

public class _18CountNumberOfDigitsAfter_Dot_InFloatingPointNumbers {
	static boolean is_integer(double k) {
		return Math.floor(k) == k;
	}

	public static void main(String[] args) {
		double num = 3.554 ;
		int counter = 0;
		while (!is_integer(num)) {
			counter++;
			num = num * 10;
		}
		System.out.println(counter);
	}
}

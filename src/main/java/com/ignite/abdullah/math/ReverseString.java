package com.ignite.abdullah.math;

public class ReverseString {
	
	public static void main(String[] args) {
		String st = "Naimul";
		
		System.out.println(reverse(st, st.length()-1));
	}
	
	
	static String reverse(String st,int n) {
		
		if (n < 0)
			return "";
		
		return st.charAt(n) + reverse(st, n-1);
		
	}

}

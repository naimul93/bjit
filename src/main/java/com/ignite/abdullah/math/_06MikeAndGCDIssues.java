package com.ignite.abdullah.math;

import java.util.Scanner;
//5
//2 3 4 9 17
//3 4 1 2 -1 
public class _06MikeAndGCDIssues {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int[] arr = new int[n];
		int[] res = new int[n];

		for (int i = 0; i < n; i++) 
			arr[i] = s.nextInt();
		

		for (int i = 0; i < n; i++) {
			int min = Integer.MAX_VALUE;

			for (int j = 0; j < n; j++) {
				if ( i!= j && gcd(arr[i], arr[j]) > 1 && Math.abs(i - j)< min) {
					min = Math.abs(i - j);
					res[i] = j+1;
				}
			}
			if (res[i] == 0)
				res[i] = res[i]-1;

		}
		
		for (int i : res) {
			System.out.print(i+ " ");
		}

	}

	static int gcd(int a, int b) {
		if (b == 0)
			return a;
		return gcd(b, a % b);
	}
}

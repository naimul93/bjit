package com.ignite.abdullah.math;

public class _15IntegerToEnglishWords {
	static String one[] = { "", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine ", "ten ",
			"eleven ", "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ",
			"nineteen " };

	static String ten[] = { "", "", "twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ",
			"ninety " };

	static String numToWords(int n, String s) {
		String str = "";
		if (n > 19)
			str += ten[n / 10] + one[n % 10];
		else
			str += one[n];
		if (n != 0)
			str += s;
		return str;
	}

	static String convertToWords(int n) {

		String out= new String();
		out += numToWords((n / 10000000), "crore ");
		out += numToWords(((n / 100000) % 100), "lakh ");
		out += numToWords(((n / 1000) % 100), "thousand ");
		out += numToWords(((n / 100) % 10), "hundred ");
		
		if ( (n > 100)  && (n % 100)== 0)
			out += "and ";
		out += numToWords((n % 100), "");
		return out;
	}

	public static void main(String[] args) {
		int n = 438237764; 
		System.out.println(convertToWords(n));
	}
}

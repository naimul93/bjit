package com.ignite.abdullah.math;

public class _13_1GCD_LCM_AndDistributiveProperty {

	static int gcd(int i, int j) {
		if (j == 0)
			return i;
		return gcd(j , i%j);
	}
	
	static int lcm(int i, int j) {
		return (i*j)/gcd(i,j);
	}

	// Returns value of GCD(LCM(x,y), LCM(x,z))
	static int findValue(int x, int y, int z) {
		int lcmxy = lcm(x, y);
		int lcmxz = lcm(x, z);
		return gcd(lcmxy, lcmxz);
	}

	public static void main(String[] args) {
		int x = 30, y = 40, z = 400;
		System.out.print(findValue(x, y, z));
	}
}

package com.ignite.abdullah.math;

public class _19BinaryToDecimal {
	static int binaryToDecimal(int n) {
		int dec = 0;

		// Initializing base value to 1, i.e 2^0
		int base = 1;
		while (n != 0) {
			int digit = n % 10;
			n /= 10;

			dec += digit * base;
			base = base * 2;
		}
		return dec;
	}

	public static void main(String[] args) {
		int num = 111;
		System.out.println(binaryToDecimal(num));
	}
}

package com.ignite.abdullah.math;

import java.util.HashMap;
import java.util.Map.Entry;

public class App {
	static int maxlenAP(int a[], int n, int d) {
		HashMap<Integer, Integer> m = new HashMap<>();
		int maxt = 1;
		for (int i = 0; i < n; i++) {
			if (m.containsKey(a[i] - i * d)) {
				int freq = m.get(a[i] - i * d);
				freq++;
				m.put(a[i] - i * d, freq);
			} else {
				m.put(a[i] - i * d, 1);
			}
		}
		for (Entry<Integer, Integer> val : m.entrySet()) {
			if (maxt < val.getValue())
				maxt = val.getValue();
		}
		return maxt;
	}

	public static void main(String[] args) {
		int n = 10, d = 3;
		int a[] = new int[] { 1, 4, 2, 5, 20, 11, 56, 100, 20, 23 };
		System.out.print(maxlenAP(a, n, d) + "\n");
	}
}

package com.ignite.abdullah.math;

public class _10PrimeString {
	static boolean isPrimeString(String str) {
		int len = str.length(), n = 0;
		for (int i = 0; i < len; i++)
			n += (int) str.charAt(i);

		System.out.println(n);

		return isPrime(2, (int) Math.sqrt(n), n);
	}

	static boolean isPrime(int i, int rt, int n) {

		if (n < 2)
			return false;
		if (n % i == 0)
			return false;
		if (i > rt)
			return true;

		return isPrime(i + 1, rt, n);

	}

	public static void main(String[] args) {
		String str = "geeksforgeeks";
		System.out.println(isPrimeString(str));
	}
}

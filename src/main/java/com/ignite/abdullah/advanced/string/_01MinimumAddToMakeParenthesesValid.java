package com.ignite.abdullah.advanced.string;

import java.util.Stack;

public class _01MinimumAddToMakeParenthesesValid {
	static int minParentheses(String S) {
		int ret = 0;
		Stack<Character> stack = new Stack<>();
		if (S == null || S.length() == 0)
			return ret;
		
		for (int i = 0; i < S.length(); ++i) {
			char curr = S.charAt(i);
			if (curr == '(')
				stack.push(curr);

			else if (curr == ')') {
				if (!stack.isEmpty() && stack.peek() == '(') {
					stack.pop();
				} else
					ret++;

			}
		}
		ret += stack.size();
		return ret;
	}

	public static void main(String args[]) {
		System.out.println(minParentheses("())"));
	}
}

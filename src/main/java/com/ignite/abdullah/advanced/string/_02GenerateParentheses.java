package com.ignite.abdullah.advanced.string;

import java.util.ArrayList;
import java.util.List;

public class _02GenerateParentheses {
	public static List<String> generateParenthesis(int n) {
		List<String> list = new ArrayList<>();
		generateParenthesis(list, "", n, n, n);
		return list;
	}

	public static void generateParenthesis(List<String> list, String perentheses, int n, int left, int right) {
		if (left == 0 && right == 0)
			list.add(perentheses);
		else {
			// if not all ( are used, we can easily add one more (
			if (left > 0)
				generateParenthesis(list, perentheses + "(", n, left - 1, right);
			// we are not allowed to add ) if our string for now is valid perentheses string
			if (left < right && right > 0)
				generateParenthesis(list, perentheses + ")", n, left, right - 1);
		}
	}

	public static void main(String[] args) {
		System.out.println(generateParenthesis(3));
	}
}

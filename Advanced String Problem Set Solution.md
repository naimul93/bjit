#Palindromes

1. Print all palindromic partitions of a string

```cpp
#include<bits/stdc++.h> 
using namespace std; 

bool isPalindrome(string str, int low, int high) 
{ 
	while (low < high) 
	{ 
		if (str[low] != str[high]) 
			return false; 
		low++; 
		high--; 
	} 
	return true; 
} 


void allPalPartUtil(vector<vector<string> >&allPart, vector<string> &currPart, 
				int start, int n, string str) 
{ 

	if (start >= n) 
	{ 
		allPart.push_back(currPart); 
		return; 
	} 

	for (int i=start; i<n; i++) 
	{ 
		
		if (isPalindrome(str, start, i)) 
		{ 
			currPart.push_back(str.substr(start, i-start+1)); 

			allPalPartUtil(allPart, currPart, i+1, n, str); 
			
			currPart.pop_back(); 
		} 
	} 
} 


void allPalPartitions(string str) 
{ 
	int n = str.length(); 

	vector<vector<string> > allPart; 

	vector<string> currPart; 

	allPalPartUtil(allPart, currPart, 0, n, str); 

	for (int i=0; i< allPart.size(); i++ ) 
	{ 
		for (int j=0; j<allPart[i].size(); j++) 
			cout << allPart[i][j] << " "; 
		cout << "\n"; 
	} 
} 

int main() 
{ 
	string str = "nitin"; 
	allPalPartitions(str); 
	return 0; 
} 
```

2. Minimum number of Appends needed to make a string palindrome

```cpp
#include<stdio.h> 
#include<string.h> 
#include<stdbool.h> 

bool isPalindrome(char *str) 
{ 
	int len = strlen(str); 

	if (len == 1) 
		return true; 

	char *ptr1 = str; 

	char *ptr2 = str+len-1; 

	while (ptr2 > ptr1) 
	{ 
		if (*ptr1 != *ptr2) 
			return false; 
		ptr1++; 
		ptr2--; 
	} 

	return true; 
} 

int noOfAppends(char s[]) 
{ 
	if (isPalindrome(s)) 
		return 0; 

	s++; 

	return 1 + noOfAppends(s); 
} 

int main() 
{ 
	char s[] = "abede"; 
	printf("%d\n", noOfAppends(s)); 
	return 0; 
} 
```

# Parantheses

# 01 MinimumAddToMakeParenthesesValid
```java
package com.ignite.abdullah.advanced.string;

import java.util.Stack;

public class _01MinimumAddToMakeParenthesesValid {
	static int minParentheses(String S) {
		int ret = 0;
		Stack<Character> stack = new Stack<>();
		if (S == null || S.length() == 0)
			return ret;
		
		for (int i = 0; i < S.length(); ++i) {
			char curr = S.charAt(i);
			if (curr == '(')
				stack.push(curr);

			else if (curr == ')') {
				if (!stack.isEmpty() && stack.peek() == '(') {
					stack.pop();
				} else
					ret++;

			}
		}
		ret += stack.size();
		return ret;
	}

	public static void main(String args[]) {
		System.out.println(minParentheses("())"));
	}
}


```

# 02 GenerateParentheses
```java
package com.ignite.abdullah.advanced.string;

import java.util.ArrayList;
import java.util.List;

public class _02GenerateParentheses {
	public static List<String> generateParenthesis(int n) {
		List<String> list = new ArrayList<>();
		generateParenthesis(list, "", n, n, n);
		return list;
	}

	public static void generateParenthesis(List<String> list, String perentheses, int n, int left, int right) {
		if (left == 0 && right == 0)
			list.add(perentheses);
		else {
			// if not all ( are used, we can easily add one more (
			if (left > 0)
				generateParenthesis(list, perentheses + "(", n, left - 1, right);
			// we are not allowed to add ) if our string for now is valid perentheses string
			if (left < right && right > 0)
				generateParenthesis(list, perentheses + ")", n, left, right - 1);
		}
	}

	public static void main(String[] args) {
		System.out.println(generateParenthesis(3));
	}
}


```

# 03ScoreOfParentheses
```java

package com.ignite.abdullah.advanced.string;

import java.util.Stack;

public class _03ScoreOfParentheses {
	static public int scoreOfParentheses(String S) {
		Stack<String> stack = new Stack<>();
		int ans = 0;
		// Solving for the whole string using stack
		for (char cc : S.toCharArray()) {
			// If opening bracket, just push it.
			if (cc == '(') {
				stack.push(cc + "");
			} else {
				// If closing bracket, then depending upon the 'type' compute sum between
				// braces,
				// and push the new result to the stack
				if (cc == ')') {
					int sum = 0;
					while (stack.size() > 0 && !stack.peek().equals("(")) {
						sum += (Integer.valueOf(stack.pop()));
					}
					stack.pop(); // remove the opening bracket now
					if (sum == 0) {
						stack.push("1");
					} else {
						stack.push(String.valueOf(2 * sum));
					}
				}
			}
		}
		// Since we may have many expressions : Eg -> ()()(())
		while (stack.size() > 0) {
			ans += Integer.valueOf(stack.pop());
		}
		return ans;
	}

	public static void main(String[] args) {
		System.out.println(scoreOfParentheses("(()(()))"));
		System.out.println(scoreOfParentheses("(())"));
		System.out.println(scoreOfParentheses("()"));
	}
}


```

# 05RemoveOutermostParentheses
```java
package com.ignite.abdullah.advanced.string;

public class _05RemoveOutermostParentheses {
	static public String removeOuterParentheses(String S) {
		if (S == "")
			return S;
		StringBuilder resultBuilder = new StringBuilder();
		char[] arr = S.toCharArray();
		int count = 0;
		int flag = 1;
		for (int i = 0; i < arr.length; ++i) {
			if (i == 0 || count == 0)
				flag = 1;
			else
				flag = 0;
			if (arr[i] == '(')
				++count;
			else if (arr[i] == ')')
				--count;
			if (flag != 1 && count != 0)
				resultBuilder.append(arr[i]);
		}
		return resultBuilder.toString();
	}

	public static void main(String[] args) {
		System.out.println(removeOuterParentheses("(()())(())"));
	}

}


```

# 
```java


```

# 
```java


```

# 
```java


```


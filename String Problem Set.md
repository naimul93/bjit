01. CamelCase
02. Strong Password
03. Two Characters
04. Caesar Cipher
05. Mars Exploration
06. HackerRank in a String!
07. Pangrams
08. Weighted Uniform Strings
09. Separate the Numbers
10. Funny String
11. Gemstones
12. Alternating Characters
13. Beautiful Binary String
14. The Love-Letter Mystery
15. Palindrome Index
16. Anagram
17. Making Anagrams
18. Game of Thrones - I
19. Two Strings
20. String Construction
21. Sherlock and the Valid String
22. Highest Value Palindrome
23. ~~Maximum Palindromes(Hard)~~
24. Sherlock and Anagrams
25. Common Child
26. Bear and Steady Gene
27. Morgan and a String
28. Minimum Window Substring

# Pattern

29. Naive algorithm for Pattern Searching
30. Search a Word in a 2D Grid of characters
31. Maximum length prefix of one string that occurs as subsequence in another
32. Find all the patterns of “1(0+)1” in a given string 
33. Replace all occurrences of string AB with C without using extra space


34. Queries to print the character that occurs the maximum number of times in a given range
35. Repeat substrings of the given String required number of times
36. Count of words ending at the given suffix in Java
37. How do you find all the permutations of a string?
38. how to remove the duplicate character from String?
39. How can a given string be reversed using recursion?
40. How do you check if two strings are a rotation of each other?
41. Rearrange the characters of the string such that no two adjacent characters are consecutive English alphabets

42. change if all bits can be made sama by single flip
43. length of longest sub-string that can be removed
44. number of flips to make binary string alterante
45. efficient method for 2's complement of a binary string
46. given a binary string, count number of substrings that start and end with 1.
47. count strings with consecutive 1's
48. add two bits strings
49. binary representation of next gerater number with same number of 1's and 0's 
50. decimal representation of given binary string is divisible by 5 or not
51. check if a binary string has a o between 1s or not 
52. ways to remove one element from a binary string that XOR becomes zero


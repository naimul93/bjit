#IQ and Aptitude Test

1.	https://www.youtube.com/watch?v=Yo_6xhG-Bcc
2.	https://www.youtube.com/watch?v=OKfwV4WJvSQ
3.	https://www.youtube.com/watch?v=PNDPDR9FwcM
4.	https://www.slideshare.net/how2become/iq-and-aptitude-test-questions-and-answers
5.	https://www.how2become.com/iq-and-aptitude-tests/
6.	https://www.jobtestprep.co.uk/free-aptitude-test
7.	https://www.wikijob.co.uk/content/aptitude-tests/test-types/aptitude-tests
8.	https://www.mon-qi.com/en/
9.	https://www.slideshare.net/how2become/iq-and-aptitude-test-questions-and-answers
10.	https://www.fibonicci.com/
11.	https://www.practiceaptitudetests.com/interview/
12.	https://www.123test.com/abstract-reasoning-test/


#Arithmetic Test

1.	https://www.monroecc.edu/depts/testingservices/preparing-for-your-test/arithmetic-sample-questions/
2.	https://www.aptitude-test.com/basic-arithmetic.html
3.	https://www.wisdomjobs.com/e-university/basic-arithmetic-interview-questions.html
4.	http://www.theonlinetestcentre.com/arithmetic-aptitude2.html
5.	https://www.algonquincollege.com/testcentre/files/2013/05/BMSA-ELSA-Sample-Questions-FINAL.pdf
6.	http://q4interview.com/sub_category.php?t=10
7.	https://career.guru99.com/top-20-mathematics-interview-question/
8.	https://www.wikijob.co.uk/content/aptitude-tests/test-types/aptitude-tests
9.	https://psychometrictests.com/aptitude-tests/numerical-aptitude-tests
10.	https://www.monroecc.edu/depts/testingservices/preparing-for-your-test/arithmetic-sample-questions/
11.	https://www.basic-mathematics.com/basic-math-test.html

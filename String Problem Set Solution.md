01. CamelCase

        Alice wrote a sequence of words in CamelCase as a string of letters, , having the following properties:
        It is a concatenation of one or more words consisting of English letters.
        All letters in the first word are lowercase.
        For each of the subsequent words, the first letter is uppercase and rest of the letters are lowercase.
        Given , print the number of words in  on a new line.
        For example, . There are  words in the string.

```java
package com.ignite.abdullah;

import java.io.IOException;
//https://www.hackerrank.com/challenges/camelcase/problem
public class _01CamelCase {

	// Complete the camelcase function below.
	static int camelcase(String s) {
		char[] ch = s.toCharArray();
		int count = 1;
		for (int i = 0; i < ch.length; i++)
			if (ch[i] >= 'A' && ch[i] <= 'Z')
				count++;

		return count;
	}

//	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//		String s = scanner.nextLine();

		System.out.println(camelcase("oneTwoThree"));

//		bufferedWriter.write(String.valueOf(result));
//		bufferedWriter.newLine();
//		bufferedWriter.close();
//		scanner.close();
	}

}

```

02. Strong Password

        Louise joined a social networking site to stay in touch with her friends. The signup page required her to input a name and a password. However, the password must be strong. The website considers a password to be strong if it satisfies the following criteria:
        Its length is at least .
        It contains at least one digit.
        It contains at least one lowercase English character.
        It contains at least one uppercase English character.
        It contains at least one special character. The special characters are: !@#$%^&*()-+
        She typed a random string of length  in the password field but wasn't sure if it was strong. Given the string she typed, can you find the minimum number of characters she must add to make her password strong?
        Note: Here's the set of types of characters in a form you can paste in your solution:


```java
package com.ignite.abdullah;

public class _02StrongPassword {

	static int minimumNumber(int n, String password) {
		int d = 0, l = 0, u = 0, s = 0;
		int length = password.length();
		for (int i = 0; i < length; i++) {
			char ch = password.charAt(i);
			if ('a' <= ch && ch <= 'z')
				l = 1;
			else if ('A' <= ch && ch <= 'Z')
				u = 1;
			else if ('0' <= ch && ch <= '9')
				d = 1;
			else
				s = 1;
		}

		int sum = d + l + u + s;
		if (sum == 4 && length >= 6)
			return 0;

		if (length < 6) {
			int missing = 4 - sum;
			if ((missing > 2) && (missing < length))
				return missing;
			else if (missing == 2 && length == 3)
				return length;
			else if (missing == 2 & length == 5)
				return missing;
			else
				return 6 - n;

		}

		return (4 - sum);

	}

	public static void main(String[] args) {
		System.out.println(minimumNumber(3, "Ab1"));
	}

}

```

03. Two Characters

        In this challenge, you will be given a string. You must remove characters until the string is made up of any two alternating characters. When you choose a character to remove, all instances of that character must be removed. Your goal is to create the longest string possible that contains just two alternating letters.

        As an example, consider the string abaacdabd. If you delete the character a, you will be left with the string bcdbd. Now, removing the character c leaves you with a valid string bdbd having a length of 4. Removing either b or d at any point would not result in a valid string.

        Given a string , convert it to the longest possible string  made up only of alternating characters. Print the length of string  on a new line. If no string  can be formed, print  instead.

        Function Description

        Complete the alternate function in the editor below. It should return an integer that denotes the longest string that can be formed, or  if it cannot be done.

        alternate has the following parameter(s):

```java
package com.ignite.abdullah;

public class _03TwoCharactersBrutForce {

	static int count_alternating(String s, char c, char d) {
		int n = 0;
		boolean last_c = false;
		boolean last_d = false;
		
		for (char a : s.toCharArray()) {
			if (a == c) {
				if (last_c)
					return 0;
				n++;
				last_c = true;
				last_d = false;
			}
			if (a == d) {
				if (last_d)
					return 0;
				n++;
				last_c = false;
				last_d = true;
			}
		}
		return n;
	}

	public static void main(String[] args) {

		String str = "beabeefeab";
		int m = 0;
		if (str.length() > 1)
			for (char i = 'a'; i <= 'z'; i++) {
				for (char d = 'a'; d <= 'z'; d++) {
					if (i == d)
						continue;
					int n = count_alternating(str, i, d);
					if (n > m)
						m = n;
				}
			}
		
		System.out.println(m);

	}
}

```

04. CaesarCipher

        The Caesar Cipher technique is one of the earliest and simplest method of encryption technique. It’s simply a type of substitution cipher, i.e., each letter of a given text is replaced by a letter some fixed number of positions down the alphabet. For example with a shift of 1, A would be replaced by B, B would become C, and so on. The method is apparently named after Julius Caesar, who apparently used it to communicate with his officials.
        Thus to cipher a given text we need an integer value, known as shift which indicates the number of position each letter of the text has been moved down.
        The encryption can be represented using modular arithmetic by first transforming the letters into numbers, according to the scheme, A = 0, B = 1,…, Z = 25.


```java
package com.ignite.abdullah;
//https://www.geeksforgeeks.org/caesar-cipher/
public class _04CaesarCipher {
    
    
    public static void main(String[] args) {
        String word = "ATTACKATONCE";
        int n = word.length();
        int k = 4;
        
        StringBuilder sb = new StringBuilder();
        
        for(int i = 0; i < n; ++i) {
            sb.append(encryptChar(word.charAt(i), k));
        }
        
        System.out.println(sb.toString());
    }
    
    private static char encryptChar(char c, int k) {
        if(!Character.isAlphabetic(c)) return c;
        int base = Character.isLowerCase(c) ? 'a' : 'A';
        return (char)((c + k - base) % 26 + base);
    }
}

```

05. MarsExploration

        Letters in some of the SOS messages are altered by cosmic radiation during transmission. Given the signal received by Earth as a string, , determine how many letters of Sami's SOS have been changed by radiation.
        For example, Earth receives SOSTOT. Sami's original message was SOSSOS. Two of the message characters were changed in transit.
        Function Description
        Complete the marsExploration function in the editor below. It should return an integer representing the number of letters changed during transmission.
        marsExploration has the following parameter(s):
        s: the string as received on Earth
        Input Format
        There is one line of input: a single string, .
        Note: As the original message is just SOS repeated  times, 's length will be a multiple of .

```java
package com.ignite.abdullah;

public class _05MarsExploration {
    static int marsExploration(String s) {

        String sos = "SOS";
        String message = s;
        int count = 0;
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) != sos.charAt(i % 3))
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(marsExploration("SOSSOS "));
    }
}

```

06. HackerRank in a String!

> https://www.hackerrank.com/challenges/hackerrank-in-a-string/problem

```c++
#include <bits/stdc++.h>

using namespace std;

// Complete the hackerrankInString function below.
string hackerrankInString(string s) {
    
    string hackerrank = "hackerrank";
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s[i] == hackerrank[index]) {
                index++;
            }
            if (index == hackerrank.length()) {
                return "YES";
            }
        }
        return "NO";


}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        string result = hackerrankInString(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
```

07. Pangrams

> https://www.hackerrank.com/challenges/pangrams/problem

    Sample Input 0
    We promptly judged antique ivory buckles for the next prize
    Sample Output 0
    pangram
    Sample Explanation 0
    All of the letters of the alphabet are present in the string.
    Sample Input 1
    We promptly judged antique ivory buckles for the prize
    Sample Output 1
    not pangram
    Sample Explanation 0
    The string lacks an x.


```c++
#include <bits/stdc++.h>
#include<string>
using namespace std;

// Complete the pangrams function below.
string pangrams(string s) {

   vector<bool>mark (26,false);
   int index;
   for(int i=0;i<s.length();i++) {
       if(s[i]>='A' && s[i]<='Z') {
           index= s[i]-'A';
       }
       else if(s[i]>='a' && s[i]<='z') {
           index= s[i]-'a';
       }
       mark[index]=true;
   }
   for(int i=0;i<26;i++) {
       if(mark[i]==false){
           return "not pangram";
       }
   }
   return "pangram";


}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = pangrams(s);

    fout << result << "\n";

    fout.close();

    return 0;
}

```

08. Weighted Uniform Strings

> https://www.hackerrank.com/challenges/weighted-uniform-string/problem

    Sample Input 0

    abccddde
    6
    1
    3
    12
    5
    9
    10
    Sample Output 0

    Yes
    Yes
    Yes
    Yes
    No
    No

```cpp
#include <bits/stdc++.h>
#include<unordered_map>
#include<vector>

using namespace std;

// Complete the weightedUniformStrings function below.
vector<string> weightedUniformStrings(string s, vector<int> queries) {
    string alphabets= "abcdefghijklmnopqrstuvwxyz";
    unordered_map<char,int> weights;
    for(int i=0;i<alphabets.length();i++) {
        weights[alphabets[i]]=i+1;
    }
    vector<int> subsWeights;
    vector<string> result;

    int mul=1;
    for(int i=0;i<s.length();i++) {
        if(s[i]==s[i-1]) 
            mul++;
        else
            mul=1;
        cout<<mul;
        subsWeights.push_back(mul*weights[s[i]]);
    }

    for(auto x: queries) {
        int flag=0;
        for(auto y: subsWeights) {
            if(x==y){
                flag=1;
                break;
            }
        }
        if(flag==1)
            result.push_back("Yes");
        else
            result.push_back("No");
    }

    return result;

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    int queries_count;
    cin >> queries_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<int> queries(queries_count);

    for (int i = 0; i < queries_count; i++) {
        int queries_item;
        cin >> queries_item;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        queries[i] = queries_item;
    }

    vector<string> result = weightedUniformStrings(s, queries);

    for (int i = 0; i < result.size(); i++) {
        fout << result[i];

        if (i != result.size() - 1) {
            fout << "\n";
        }
    }

    fout << "\n";

    fout.close();

    return 0;
}
```

}


09. SeparateTheNumbers

> https://www.hackerrank.com/challenges/separate-the-numbers/problem

Sample Input 1

    4
    99910001001
    7891011
    9899100
    999100010001
    Sample Output 1

    YES 999
    YES 7
    YES 98
    NO

```java
package com.ignite.abdullah;

public class _09SeparateTheNumbers {
	static void separateNumbers(String s) {

		var str = s;
		var numLen = str.length();
		var firstNum = 0;
		var incre = 0;
		var testStr = "";

		if (numLen <= 1) {
			System.out.println("NO");
			return ;

		}
		for (var i = 1; i <= numLen / 2; i++) {
			firstNum = Integer.parseInt(str.substring(0, i));
			testStr = String.valueOf(firstNum);
			incre = firstNum;

			while (testStr.length() < numLen && !(str.equals(testStr))) {
				incre++;
				testStr += String.valueOf(incre);
			}

			if (testStr.equals(str))
				break;

		}

		String res = testStr.equals(str) ? "YES " + firstNum : "NO";
		System.out.println(res);

	}

	public static void main(String[] args) {
		separateNumbers("1234");
	}
}
```

10. Funny String

> https://www.hackerrank.com/challenges/funny-string

    Sample Input

    2
    acxz
    bcxz
    Sample Output

    Funny
    Not Funny

```cpp
#include <bits/stdc++.h>
#include<vector>
using namespace std;

// Complete the funnyString function below.
string funnyString(string s) {
    vector<int> sdiff;
    vector<int> rdiff;
    for(int i=0;i<s.length()-1;i++) {
        sdiff.push_back(abs(s[i+1]-s[i]));
    }
    for(int i=s.length()-1;i>0;i--) {
        rdiff.push_back(abs(s[i-1]-s[i]));
    }
    for(int i=0;i<sdiff.size();i++) {
        if(sdiff[i]!=rdiff[i])
            return "Not Funny";
    }
    return "Funny";

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        string result = funnyString(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
```


11.  Gemstones

> https://www.hackerrank.com/challenges/gem-stones

    Sample Input

    3
    abcdde
    baccd
    eeabg
    Sample Output

    2

```java
package com.ignite.abdullah.string;

public class _11Gemstones {

    static int gemstones(String[] arr) {
        int n = arr.length;
        char ch = 'a';
        int[] a = new int[26];
        int count = 0;
        for (int i = 0; i < n; i++) {
            String s = arr[i];
            for (int j = 0; j < s.length(); j++)
                if (a[s.charAt(j) % ch] == i)
                    a[s.charAt(j) % ch]++;
        }
        for (int i = 0; i < 26; i++)
            if (a[i] == n)
                count++;
        return count;
    }

    public static void main(String[] args) {
        System.out.println(gemstones(new String[] { "abcdde", "baccd", "eeabg" }));
    }
}

```

12. AlternatingCharacters

> https://www.hackerrank.com/challenges/alternating-characters/problem

    Sample Input

    5
    AAAA
    BBBBB
    ABABABAB
    BABABA
    AAABBB
    Sample Output

    3
    4
    0
    0
    4


```java
package com.ignite.abdullah;

public class _12AlternatingCharacters {
    static int alternatingCharacters(String s){
        int count = 0;
        for(int i = 0; i < s.length() - 1; i++) {
            if(s.charAt(i) == s.charAt(i + 1))
                count++;
        }
            
        return count;
    }
    
    public static void main(String[] args) {
        System.out.println(alternatingCharacters("ABABABAB"));
    }

}

```

13. BeautifulBinaryString

> https://www.hackerrank.com/challenges/beautiful-binary-string/problem

    Sample Input 0

    7
    0101010
    Sample Output 0

    2  

```java
package com.ignite.abdullah.string;

public class _13BeautifulBinaryString {
    // Complete the beautifulBinaryString function below.
    static int beautifulBinaryString(String b) {
        int count = 0;
        char[] arr = b.toCharArray();
        for (int i = 2; i < b.length(); i++) {
            if (arr[i] == '0' && arr[i - 2] == '0' && arr[i - 1] == '1') {
                count++;
                i += 2;
            }
        }
        return count;

    }

    public static void main(String[] args) {
        System.out.println(beautifulBinaryString("0100101010"));
    }
}

```

14. TheLoveLetterMystery

        James found a love letter that his friend Harry has written to his girlfriend. James is a prankster, so he decides to meddle with the letter. He changes all the words in the letter into palindromes.
        To do this, he follows two rules:
        He can only reduce the value of a letter by , i.e. he can change d to c, but he cannot change c to d or d to b.
        The letter a may not be reduced any further.
        Each reduction in the value of any letter is counted as a single operation. Find the minimum number of operations required to convert a given string into a palindrome.
        For example, given the string , the following two operations are performed: cde → cdd → cdc.

```java
package com.ignite.abdullah.string;

public class _14TheLoveLetterMystery {

    // Complete the theLoveLetterMystery function below.
    static int theLoveLetterMystery(String s) {
        int oper = 0, n = s.length();
        for (int i = 0, j = n - 1; i < n / 2; i++, j--)
            oper += Math.abs(s.charAt(i) - s.charAt(j));
        return oper;
    }

    public static void main(String[] args) {

        System.out.println(theLoveLetterMystery("abcd"));
    }
}

```

15. PalindromeIndex

        Given a string of lowercase letters in the range ascii[a-z], determine a character that can be removed to make the string a palindrome. There may be more than one solution, but any will do. For example, if your string is "bcbc", you can either remove 'b' at index 0 or 'c' at index 3. If the word is already a palindrome or there is no solution, return -1. Otherwise, return the index of a character to remove.

```java
package com.ignite.abdullah.string;

public class _15PalindromeIndex {
    static int palindromeIndex(String s) {
        for (int i = 0, j = s.length() - 1; i < j; i++, j--)
            if (s.charAt(i) != s.charAt(j))
                if (isPalindrome(s, i))
                    return i;
                else if (isPalindrome(s, j))
                    return j;
        return -1;
    }

    static boolean isPalindrome(String s, int index) {
        for (int i = index + 1, j = s.length() - 1 - index; i < j; i++, j--)
            if (s.charAt(i) != s.charAt(j))
                return false;
        return true;
    }

    public static void main(String[] args) {
        System.out.println(palindromeIndex("aaab"));
    }
}

```

16. Anagram

> https://www.hackerrank.com/challenges/anagram

```cpp
#include <iostream>
#include<algorithm>
using namespace std;

bool anagram(string &s1, string &s2) {
	sort(s1.begin(),s1.end());
	sort(s2.begin(),s2.end());
	if(s1.length()!=s2.length())
		return false;
	for(int i=0;i<s1.length();i++) {
		if(s1[i]!=s2[i])
			return false;
	}
	return true;
}

int main() {
	string s1="Naimul";
	string s2="mulNai";
	anagram(s1,s2)?cout<<"Anagram":cout<<"Not anagram";
	
	return 0;
}
```
17. Making Anagrams

> https://www.hackerrank.com/challenges/making-anagrams/problem

    Sample Input
    cde
    abc
    Sample Output
    4

```cpp
#include <bits/stdc++.h>
#include <map>
using namespace std;

// Complete the makingAnagrams function below.
int makingAnagrams(string s1, string s2) {
    map<char, int> map1;
    map<char, int> map2;
    for(int i=0;i<s1.length();i++) {
        map1[s1[i]]++;
    }
    for(int j=0;j<s2.length();j++) {
        map2[s2[j]]++;
    }
    int count=0;
    for(auto x: map1) {
        for(auto y: map2) {
            if(x.first==y.first) {
                count+=2*min(x.second, y.second);
            }
        }
    }
    return (s1.length()+s2.length())-count;

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s1;
    getline(cin, s1);

    string s2;
    getline(cin, s2);

    int result = makingAnagrams(s1, s2);

    fout << result << "\n";

    fout.close();

    return 0;
}
```

18. Game of Thrones - I

> https://www.hackerrank.com/challenges/game-of-thrones/problem

    Sample Input 0

    aaabbbb
    Sample Output 0

    YES


```cpp
#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

// Complete the gameOfThrones function below.
string gameOfThrones(string s) {
    int size= s.length();
    unordered_map<char, int> umap;
    for(int i=0;i<size;i++){
        umap[s[i]]++;
    }
    int oddcount=0;
    for(auto x: umap){
        if(x.second%2==1)
            oddcount++;
    }
    if(size%2==1 && oddcount==1)
        return "YES";
    else if(size%2==0 && oddcount==0)
        return "YES";
    else
        return "NO";
    


}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = gameOfThrones(s);

    fout << result << "\n";

    fout.close();

    return 0;
}
```
19. Two Strings

> https://www.hackerrank.com/challenges/two-strings/problem

    Sample Input

    2
    hello
    world
    hi
    world
    Sample Output

    YES
    NO


```cpp
#include <bits/stdc++.h>
#include <unordered_map>

using namespace std;

// Complete the twoStrings function below.
string twoStrings(string s1, string s2) {
    unordered_map<char, int> umap;
    for(int i=0;i<s1.length();i++){
        umap[s1[i]]++;
    }
    for(int j=0;j<s2.length();j++){
        if(umap[s2[j]]>0)
            return "YES";
    }
    return "NO";

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s1;
        getline(cin, s1);

        string s2;
        getline(cin, s2);

        string result = twoStrings(s1, s2);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
```

20. String Construction

> https://www.hackerrank.com/challenges/string-construction/problem

    Sample Input

    2
    abcd
    abab
    Sample Output

    4
    2


```cpp
#include <bits/stdc++.h>
#include<set>
using namespace std;

// Complete the stringConstruction function below.
int stringConstruction(string s) {
    set<char> distincts;
    for(int i=0;i<s.length();i++) {
        distincts.insert(s[i]);
    }
    int counter=0;
    return distincts.size();

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        int result = stringConstruction(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
```

# 21. Sherlock and the Valid String
```cpp
#include <bits/stdc++.h>
#include <map>
#include<iterator>
#include <vector>

using namespace std;

// Complete the isValid function below.
string isValid(string s) {
    const char * s_chars = s.c_str();
    vector<int> occur(26);
    
    for(int i = 0; i < s.length(); i++ ){
        occur[s_chars[i] - 'a']++;
    }
    
    int max_occur =-1;
    bool removed_char = false;
    for(int i =0; i < 26; i++){
        if(occur[i] == 0){
            continue;
        }else if(max_occur == -1){
            max_occur = occur[i];
            continue;
        }else if(occur[i] == max_occur){
            continue;
        }else if(!removed_char && (occur[i] == max_occur + 1 || occur[i] == 1)){
            removed_char = !removed_char;
            continue;  
        }else{
            return "NO";
        }
    }
    
    return "YES";

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = isValid(s);

    fout << result << "\n";

    fout.close();

    return 0;
}
```

# 22. Highest Value Palindrome
```java
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static boolean isPalindrome(String s){
        int i = 0;
        int j = s.length()-1;

        while(j > i){
            if (s.charAt(i) == s.charAt(j)){
                i++; j--;
            } else {
                return false;
            }
        }
        return true;
}

    // Complete the highestValuePalindrome function below.
    static String highestValuePalindrome(String s, int n, int k) {
        int lo = 0;
        int hi = n-1;;
        char[] string = s.toCharArray();
        int diff = 0;

        for(int i=0, j=n-1; i<n/2; i++, j--){
            if (string[i] != string[j]){
                diff++;
            }
        }

        if (diff > k){
            return "-1";
        }

        while(hi >= lo){
            if (k <= 0){
                break;
            }

            if (string[lo] == string[hi]){
                if (k > 1 && (k-2) >= diff && string[lo] != '9'){
                    string[lo] = '9';
                    string[hi] = '9';
                    k-=2;
                }
            }
            else {
                if (k > 1 && (k-2) >= diff-1){
                    if (string[lo] != '9'){
                        string[lo] = '9';
                        k--;
                    }
                    if (string[hi] != '9'){
                        string[hi] = '9';
                        k--;
                    }
                } else {
                    if (string[lo] > string[hi]){
                        string[hi] = string[lo];
                    } else {
                        string[lo] = string[hi];
                    }
                    k--;
                }
                diff--;
            }
            if (lo == hi && k > 0){
                string[lo] = '9';
                k--;
            }
            lo++;
            hi--;
        }

        s = String.valueOf(string);
        return isPalindrome(s) ? s : "-1";

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        String s = scanner.nextLine();

        String result = highestValuePalindrome(s, n, k);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
```

# 24. Sherlock and Anagrams
    https://www.hackerrank.com/challenges/sherlock-and-valid-string/problem
```cpp
#include <bits/stdc++.h>
#include<map>
#include <algorithm>
#include<string>

using namespace std;

// bool anagram(string &s1, string &s2) {
//     sort(s1.begin(),s1.end());
//     sort(s2.begin(),s2.end());
//     if(s1.length()!=s2.length())
//         return false;
//     for(int i=0;i<s1.length();i++) {
//         if(s1[i]!=s2[i])
//             return false;
//     }
//     return true;
// }

// Complete the sherlockAndAnagrams function below.
int sherlockAndAnagrams(string s) {
    map<string,int> allSubs;
    int n= s.length();
    
    for(int i=0;i<n;i++) {
        for(int len=1;len<=n-i;len++) {
            string t=(s.substr(i,len));
            sort(t.begin(), t.end());
            allSubs[t]++;
        } 
    }
    long long count=0;
    for(auto x: allSubs) {
        count+=(long long)(x.second)*(x.second-1)/2;
    }
    return count;

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        int result = sherlockAndAnagrams(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
```

# 25. Common Child
    https://www.hackerrank.com/challenges/common-child/problem
```cpp
#include <bits/stdc++.h>

using namespace std;

// Complete the commonChild function below.
int commonChild(string s1, string s2) {
     // We need to use a staticly allocated array or we will run out of stack space (results in a segmentation fault)
    static int array[5000][5000];
    // Initialize array
    for(int i = 0; i < s1.length(); i++) {
       for(int j = 0; j < s2.length(); j++) {
           array[i][j] = 0;
       }
    }
    // Use dynamic programming to determine the size of the largest subsequence
    // We can break the problem down into parts by considering each string, character by character
    // If the two strings have the same char at the same index, then the largest subsequence at that point
    // is simply the largest subsequence before that char was added to either string + 1
    // If the two strings DON'T have the same char at the same index, then the largest subsequence at that point
    // is the larger of either a) string1 without that char or b) string2 without that char 
    for(int i = 0; i < s1.length(); i++) {
        for(int j = 0; j < s2.length(); j++) {
            if(s1[i] == s2[j]) {
                // Check if a diagonal cell (a smaller subsequence) exists 
                // If it does, increment by 1 + the value of the cell (size of the previous subsequence)
                // Otherwise set the cell equal to 1 
                if(i > 0 && j > 0) {
                    array[i][j] = array[i-1][j-1] + 1;
                } else {
                    array[i][j] = 1;
                }
            } else if(i == 0 && j > 0) {
                // Take the cell above
                array[i][j] = array[i][j-1];
            } else if(i > 0 && j == 0) {
                // Take the cell to the left
                array[i][j] = array[i-1][j];
            } else if(i > 0 && j > 0) {
                // Take the bigger of the cell above or to the left
                if(array[i][j-1] > array[i-1][j]) {
                    array[i][j] = array[i][j-1];
                } else {
                    array[i][j] = array[i-1][j];
                }
            }
        }
    }
    return array[s1.length()-1][s2.length()-1];

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s1;
    getline(cin, s1);

    string s2;
    getline(cin, s2);

    int result = commonChild(s1, s2);

    fout << result << "\n";

    fout.close();

    return 0;
}
```

26. BearAndSteadyGene

> https://www.hackerrank.com/challenges/bear-and-steady-gene/problem
```java
package com.ignite.abdullah.string;

import java.util.HashMap;
import java.util.Map;

public class _26BearAndSteadyGene {
    static int steadyGene(String gene) {

        String s = gene;
        int n = s.length(), left = 0, right = 0, min = Integer.MAX_VALUE;
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 0);
        map.put('C', 0);
        map.put('G', 0);
        map.put('T', 0);
        
        
        for (char c : s.toCharArray())
            map.put(c, map.get(c) + 1);

        while (right < n - 1) {
            char rc = s.charAt(right++);
            map.put(rc, map.get(rc) - 1);
            while (isSteady(map, n)) {
                min = Math.min(min, right - left);
                char lc = s.charAt(left++);
                map.put(lc, map.get(lc) + 1);
            }
        }
        return min;

    }

    private static boolean isSteady(Map<Character, Integer> map, int n) {
        for (Integer i : map.values())
            if (i > n / 4)
                return false;
        return true;
    }

    public static void main(String[] args) {
        System.out.println(steadyGene("GAAATAAA"));
    }
}

```

27. Morgan And A String

```java
package com.ignite.abdullah.string;

public class _27MorganAndAString {

    static String morganAndString(String a, String b) {
        var result = new StringBuilder(a.length() + b.length());
        var i = 0;
        var j = 0;

        while (i < a.length() && j < b.length()) {
            if (breakTie(a, b, i, j) == 1) {
                var bb = b.charAt(j);
                while (j < b.length() && bb == b.charAt(j)) {
                    result.append(bb);
                    j++;
                }
            } else {
                var aa = a.charAt(i);
                while (i < a.length() && aa == a.charAt(i)) {
                    result.append(aa);
                    i++;
                }
            }
        }

        result.append(a.substring(i));
        result.append(b.substring(j));

        return result.toString();

    }

    static int breakTie(String a, String b, int i, int j) {
        while (i < a.length() && j < b.length()) {
            if (a.charAt(i) < b.charAt(j))
                return -1;
            if (a.charAt(i) > b.charAt(j))
                return 1;
            i++;
            j++;
        }
        return i < a.length() ? -1 : 1;
    }

    public static void main(String[] args) {
        System.out.println(morganAndString("JACK", "DANIEL"));
    }

}

```



28. 28MinimumWindowSubstring

        Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
        Example:
        Input: S = "ADOBECODEBANC", T = "ABC"
        Output: "BANC"
        Note:
        If there is no such window in S that covers all characters in T, return the empty string "".
        If there is such window, you are guaranteed that there will always be only one unique minimum window in S.


```java
package com.ignite.abdullah;

public class _28MinimumWindowSubstring {
    public static String minWindow(String s, String t) {
        if (s.isEmpty())
            return "";
        int[] freq = new int[256];
        for (int i = 0; i < t.length(); i++)
            freq[t.charAt(i)]++;

        char[] arr = s.toCharArray();
        int l = 0, r = 0, missing = t.length(), i = 0, j = 0;
        while (r < s.length()) {
            if (freq[arr[r]] > 0)
                missing--;
            freq[arr[r]]--;
            r++;
            while (missing == 0) {
                if (j == 0 || (r - l) < (j - i)) {
                    j = r;
                    i = l;
                }
                freq[arr[l]]++;
                if (freq[arr[l]] > 0)
                    missing++;
                l++;
            }
        }
        return s.substring(i, j);
    }

    public static void main(String[] args) {
        System.out.println(minWindow("a test string", "tist"));
    }
}
/****
Smallest window is :
t stri
*/
```

![Alt-Text](/images/smallestWindow.jpg)


29. NaiveAlgorithmForPatternSearching

```java
package com.ignite.abdullah.string;

public class _29NaiveAlgorithmForPatternSearching {

    public static void search(String txt, String pat) {
        int m = txt.length();
        int n = pat.length();
        for (int i = 0; i <= m - n; i++) {
            int j;
            for (j = 0; j < n; j++)
                if (txt.charAt(i + j) != pat.charAt(j))
                    break;
            if (j == n)
                System.out.println("Pattern found at index " + i);
        }
    }

    public static void main(String[] args) {
        String txt = "AABAACAADAABAAABAA";
        String pat = "AABA";
        search(txt, pat);
    }
}

```


30. SearchAWordInA2DGridOfCharacters

```java
package com.ignite.abdullah.string;

public class _30SearchAWordInA2DGridOfCharacters {
    static int R, C;

    static int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
    static int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };

    // This function searches in all 8-direction from point
    // (row, col) in grid[][]
    static boolean search2D(char[][] grid, int row, int col, String word) {
        // If first character of word doesn't match with
        // given starting point in grid.
        if (grid[row][col] != word.charAt(0))
            return false;

        int len = word.length();

        // Search word in all 8 directions
        // starting from (row,col)
        for (int dir = 0; dir < 8; dir++) {
            // Initialize starting point
            // for current direction
            int k, rd = row + x[dir], cd = col + y[dir];

            // First character is already checked,
            // match remaining characters
            for (k = 1; k < len; k++) {
                // If out of bound break
                if (rd >= R || rd < 0 || cd >= C || cd < 0)
                    break;

                // If not matched, break
                if (grid[rd][cd] != word.charAt(k))
                    break;

                // Moving in particular direction
                rd += x[dir];
                cd += y[dir];
            }

            // If all character matched, then value of must
            // be equal to length of word
            if (k == len)
                return true;
        }
        return false;
    }

    static void patternSearch(char[][] grid, String word) {
        for (int row = 0; row < R; row++) {
            for (int col = 0; col < C; col++) {
                if (search2D(grid, row, col, word))
                    System.out.println("pattern found at " + row + ", " + col);
            }
        }
    }

    // Driver code
    public static void main(String args[]) {
        R = 3;
        C = 13;
        char[][] grid = { { 'G', 'E', 'E', 'K', 'S', 'F', 'O', 'R', 'G', 'E', 'E', 'K', 'S' },
                { 'G', 'E', 'E', 'K', 'S', 'Q', 'U', 'I', 'Z', 'G', 'E', 'E', 'K' },
                { 'I', 'D', 'E', 'Q', 'A', 'P', 'R', 'A', 'C', 'T', 'I', 'C', 'E' } };
        patternSearch(grid, "GEEKS");
//      System.out.println();
//      patternSearch(grid, "EEE");
    }
}
```



31. MaximumLengthPrefixOfOneStringThatOccursAsSubsequenceInAnother

```java
package com.ignite.abdullah.string;

public class _31MaximumLengthPrefixOfOneStringThatOccursAsSubsequenceInAnother {
	static int maxPrefix(String s, String t) {
		int count = 0;
		for (int i = 0; i < t.length(); i++) {
			if (count == t.length())
				break;
			if (t.charAt(i) == s.charAt(count))
				count++;
		}
		return count;
	}

	public static void main(String args[]) {
		String S = "digger";
		String T = "biggerdiagram";
		System.out.println(maxPrefix(S, T));
	}
}

```

33. ReplaceAllOccurrencesOfString_AB_With_C_WithoutUsingExtraSpace

```java
package com.ignite.abdullah.string;

public class _33ReplaceAllOccurrencesOfString_AB_With_C_WithoutUsingExtraSpace {

	static void translate(char str[]) {
		for (int i = 1; i < str.length; i++) {
			if (str[i - 1] == 'A' && str[i] == 'B') {
				str[i - 1] = 'C';
				int j;
				for (j = i; j < str.length - 1; j++)
					str[j] = str[j + 1];
				str[j] = ' ';
			}
		}
		return;
	}

	public static void main(String args[]) {
		String st = "helloABworldABGfG";
		char str[] = st.toCharArray();
		translate(str);
		System.out.println(str);
	}

}

```


34. QueriesToPrintTheCharacterThatOccursTheMaximumNumberOfTimesInAGivenRange

```java
package com.ignite.abdullah.string;

public class _34QueriesToPrintTheCharacterThatOccursTheMaximumNumberOfTimesInAGivenRange {

	static void solveQueries(String str, int start, int end) {

		String temp = str.substring(start, end + 1);

		int[] arr = new int[256];

		for (int i = 0; i < temp.length(); i++) {
			arr[(temp.charAt(i))]++;
		}

		int max = 0;
		int res = 0;
		for (int i = 0; i < arr.length; i++) {

			if (arr[i] > max) {
				max = arr[i];
				res = i;
			}
		}

		System.out.println((char)(res));

	}

	public static void main(String[] args) {
		String str = "striver";

//	    query.push_back({ 0, 1 }); 
//	    query.push_back({ 1, 6 }); 
//	    query.push_back({ 5, 6 }); 

		solveQueries(str, 1, 6);
	}
}

```


37. HowDoYouFindAllThePermutationsOfAString

```java
package com.ignite.abdullah.string;

public class _37HowDoYouFindAllThePermutationsOfAString {

    static char[] words = { 'a', 'b', 'c' };
    static int length = words.length;
    static int counting = 0;

    public static void anargam(int elements) {
        if (elements == 1) {
            counting++;
            // print the word
            System.out.print(words);
            System.out.println(" " + counting);
            return;
        }
        for (int i = 1; i <= elements; i++) {

            int temp = elements - 1;

            anargam(temp);

            rotate(elements);
            // System.out.println("temp : " + temp + " i : " + i + " Elements : " + elements
            // +" Words : "+ String.valueOf(words));
        }
        // System.out.println("For loop end");
    }

    public static void rotate(int elements) {
        int startingIndex = length - elements;
        char tmp = words[startingIndex];

        for (int i = startingIndex; i <= length - 2; i++) {
            words[i] = words[i + 1];
        }
        words[length - 1] = tmp;
    }

    public static void main(String[] args) {
        anargam(words.length);
    }

}

```
![Alt-Text](/images/permutation.jpg)


38. RemoveDuplicatesFromAGivenString 

```java
package com.ignite.abdullah.string;

import java.util.HashSet;
import java.util.Set;

public class _38RemoveDuplicatesFromAGivenString {

    static void removeDuplicates(String str) {
        Set<Character> lhs = new HashSet<>();
        for (int i = 0; i < str.length(); i++)
            lhs.add(str.charAt(i));

        for (Character ch : lhs)
            System.out.print(ch);
    }

    public static void main(String args[]) {
        String str = "cmabdullah";
        removeDuplicates(str);
    }
}
```

39. HowCanAGivenStringBeReversedUsingRecursion
```java
package com.ignite.abdullah.string;

public class _39HowCanAGivenStringBeReversedUsingRecursion {

    public static void main(String[] args) {
        reverse("abdullah");
    }

    private static void reverse(String string) {
        
        if (string == null || string.isEmpty())
            return ;
        System.out.print(string.charAt(string.length()-1));
        reverse(string.substring(0, string.length()-1));
        
    }

}

```


40. HowDoYouCheckIfTwoStringsAreARotationOfEachOther

```java
package com.ignite.abdullah.string;

public class _40HowDoYouCheckIfTwoStringsAreARotationOfEachOther {

    static boolean areRotations(String str1, String str2) {
        return (str1.length() == str2.length()) && ((str1 + str1).indexOf(str2) != -1);
    }

    public static void main(String[] args) {
        String str1 = "AACD";
        String str2 = "ACDA";

        System.out.println(areRotations(str1, str2));
    }

}

```


37. 

```java

```


37. 

```java

```


37. 

```java

```


37. 

```java

```



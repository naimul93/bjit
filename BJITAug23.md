
01. DuplicateNode

```java
package com.ignite.abdullah;

public class _01DuplicateNode {
	public static Node head;

	static class Node {

		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	static void removeDuplicates() {

		if (head == null || head.next == null) {
			return;
		}
		Node current = head;

		while (current != null && current.next != null) {
			if (current.data == current.next.data) {
				current.next = current.next.next;
			} else {
				current = current.next;
			}
		}
	}

	static void display(Node head) {
		Node current = head;
		while (current != null) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.println(current);
	}

	public static void main(String[] args) {

		head = new Node(10);
		head.next = new Node(11);
		head.next.next = new Node(12);
		head.next.next.next = new Node(12);
		head.next.next.next.next = new Node(12);
		head.next.next.next.next.next = new Node(13);
		head.next.next.next.next.next.next = new Node(13);

		display(head);

		removeDuplicates();

		display(head);
	}
}
```



02. RecursionList

```java
package com.ignite.abdullah;

import java.util.Stack;

public class _02RecursionList {

	public static void main(String[] args) {

		int n = 123;
		Stack<Integer> st = new Stack<>();
		Stack<Integer> res = recurse(n, st);
		
//		System.out.println(res.get(0));
		

		
		System.out.print(res.pop() + " ");
		System.out.print(res.pop() + " ");
		System.out.print(res.pop() + " ");

	}

	static Stack<Integer> recurse(int n, Stack<Integer> st) {

		if (n == 0)
			return st;

		int mod = n % 10;

		st.push(mod);
		n = n / 10;
		return recurse(n, st);
	}
}
```

03. Abbriviation
```java
package com.ignite.abdullah;

public class _03Abbriviation {

	public static void main(String[] args) {
		String st = "Bangladesh Japan Information Technology";
		
		
		String res = new String();
		

		
		for (int i = 0 ; i< st.length() ; i++) {
			char c = st.charAt(i);
			if (c >= 'A' && c <= 'Z') {
				res += c;
			}
		}
		
		System.out.println(res);

		
	}

}
```

04. StqckAndQueue

```java
package com.ignite.abdullah;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

public class _04StqckAndQueue {

	public static void main(String[] args) {
		String name = "cmamc";
		System.out.println(checkPalindrome(name));
	}

	static boolean checkPalindrome(String name) {
		Stack<Character> st = new Stack<>();
		Queue<Character> queue = new ArrayBlockingQueue<>(name.length());

		for (int i = 0; i < name.length(); i++)
			st.push(name.charAt(i));

		for (int i = 0; i < name.length(); i++)
			queue.add(name.charAt(i));

		for (int i = 0; i < name.length(); i++)
			if (st.pop() != queue.poll())
				return false;

		return true;
	}
}
```

05. Array

```java
package com.ignite.abdullah;

import java.util.Arrays;

public class _05Array {

	public static void main(String[] args) {
		int[] arr = { 2, 3, 4, 6, 9, 7, 8 };
		int count = 0;
		Arrays.sort(arr);
		
//		for (int i : arr) {
//			System.out.print(i + " ");
//		}
		
		for (int i = 2 ; i< arr.length ; i++) {
			
			int a = arr[i-2]+2;
			int b = arr[i-1]+1;
			int c = arr[i];
			
			if (a == b && b == c)
				count++;
		}
		
		System.out.println(count);
	}
}
```

# Object Analysis


### Eye

```java
package com.ignite.abdullah.object.analysis;

public class Eye {
	
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Eye [color=" + color + "]";
	}

}

```

### Nose

```java
package com.ignite.abdullah.object.analysis;

public class Nose {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Nose [type=" + type + "]";
	}
	
	
}

```

### Animal

```java
package com.ignite.abdullah.object.analysis;

public class Animal {
	
	private String animalName;
	
	public String getAnimalName() {
		return animalName;
	}
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}
	public void  eat() {
		System.out.println(animalName + " is eating...");
	}
	public void  run() {
		System.out.println(animalName+ " is running...");
	}
	@Override
	public String toString() {
		return "Animal [animalName=" + animalName + "]";
	}
}

```

### Cow

```java
package com.ignite.abdullah.object.analysis;

public class Cow extends Animal{

	private Eye eye;
	private Nose nose;
	
	public Eye getEye() {
		return eye;
	}
	public void setEye(Eye eye) {
		this.eye = eye;
	}
	public Nose getNose() {
		return nose;
	}
	public void setNose(Nose nose) {
		this.nose = nose;
	}
	@Override
	public String toString() {
		return "Cow [eye=" + eye + ", nose=" + nose + "]";
	}
	
}

```

### App

```java
package com.ignite.abdullah.object.analysis;

public class App {

	
	public static void main(String[] args) {
		
		Eye eye = new Eye();
		eye.setColor("Blue");
		
		Nose nose = new Nose();
		nose.setType("Fede");
		
		Cow cow = new Cow();
		cow.setEye(eye);
		cow.setNose(nose);
		cow.setAnimalName("Cow");
		
		cow.eat();
		cow.run();
//		System.out.println(cow.toString());
		
	}
}

```

```java

```

```java

```

```java

```



01. Find a pair with maximum product in array of Integers
02. Reverse the names and then sort them in lexicographical order.
03. Equilibrium index of an array
04. Recursive function to check if a string is palindrome
05. Check whether two strings are anagram of each other
06. Count number of digits after `.` in floating point numbers?
07. program to count the occurrences of each character
08. Write program to find the years from a sentence
09. LRU Cache Implementation
10. This challenge requires you to determine if a given number is a 
11. Print All Distinct Elements of a given integer array
12. Reverse the words of a sentence
13. Find the Missing Number
14. Have the function MaximalSquare(strArr)
15. Have the function PentagonalNumber(num) 
16. ~~Have the function EightQueens(strArr) read strArr which will be an array consisting of the locations of eight Queens on a standard 8x8 chess board with no other pieces on the board.~~
17. Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string. 
18. Have the function ClosestEnemyII(strArr) read the matrix of numbers stored in strArr which will be a 2D matrix that contains only the integers 1, 0, or 2. 
19.  Median of Two Sorted Arrays
20. Remove Duplicates from Sorted Array
21. Rotate Array
22. Valid Parentheses
23. Find the minimum number of platforms required for railway station
24. Find a Pair Whose Sum is Closest to zero in Array
25. Find all pairs of elements from an array whose sum is equal to given number
26. Given an array of 0’s and 1’s in random order, you need to separate 0’s and 1’s in an array.
27. Separate odd and even numbers in an array
28. Given an array containing zeroes, ones and twos only. Write a function to sort the given array in O(n) time complexity.
29. Find local minima in array
30. Sliding window maximum in java
31. ~~kth smallest element in a row-wise and column-wise sorted 2d array(Hard)~~
32. ~~kth smallest/Largest element in unsorted array(Hard)~~
33. find all elements in array which have at-least two greater elements
34. find k pairs with smallest sums in two arrays
35. find the smallest and second smallest elements in an array
36. find the smallest missing number
37. find the minimum distance between tow numbers
38. find the smallest positive number missing from an unsorted array
39. find the closest pair from tow sorted arrays
40. find the largest pair sum in an unsorted array
41. print the pattern using only a single loop: 98765432123456789
42. Find smallest range with at-least one element from each of the given lists
43. Print all Triplets that forms Arithmetic Progression
44. Reverse every consecutive m elements of the given subarray
45. Find the smallest window in array sorting which will make the entire array sorted
46. Rearrange the array with alternate high and low elements
47. Rod Cutting
48. find-minimum-maximum-element-array-using-minimum-comparisons
49. Find the surpasser count for each element of an array
50. Find numbers represented as sum of two cubes for two different pairs



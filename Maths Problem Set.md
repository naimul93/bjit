1. Anshuli and the Party
2. Gotta Catch 'Em All
3. AREA21
4. GCD and Fibonacci Numbers
5. Calculate the Power
6. Mike and GCD Issues
7. Efficient program to print all prime factors of a given number
8. Nearest Prime
9. Find largest prime factor of a number
10. Prime String
11. Least Prime Factor
12. Sum Of Digit Is Palindrome Or Not
13. GCD, LCM and Distributive Property compute the value of GCD(LCM(x,y), LCM(x,z)).
14. k-th prime factor of a given number
15. Integer to English Words
16. log/sin/cos/tan/cot/


# Some common formulas

1. Sum of 1 to N = (n+1) * n/2
2. Sum of GP = 2⁰ + 2¹ + 2² + 2³ + … 2^n = 2^(n+1)-1
3. Permutations of N = N! / (N-K)!
4. Combinations of N = N! / (K! * (N-K)!)
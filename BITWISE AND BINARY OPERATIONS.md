1. Program for Binary To Decimal Conversion

	Input : 111
	Output : 7

	Input : 1010
	Output : 10

	Input: 100001
	Output: 33

For Example:
If the binary number is 111.
dec_value = 1*(2^2) + 1*(2^1) + 1*(2^0) = 7

// C++ program to convert binary to decimal 
```cpp
#include <iostream> 
using namespace std; 

// Function to convert binary to decimal 
int binaryToDecimal(int n) 
{ 
	int num = n; 
	int dec_value = 0; 

	// Initializing base value to 1, i.e 2^0 
	int base = 1; 

	int temp = num; 
	while (temp) { 
		int last_digit = temp % 10; 
		temp = temp / 10; 

		dec_value += last_digit * base; 

		base = base * 2; 
	} 

	return dec_value; 
} 

// Driver program to test above function 
int main() 
{ 
	int num = 10101001; 

	cout << binaryToDecimal(num) << endl; 
} 
```

2. Program for Decimal to Binary Conversion

Input : 7
Output : 111

Input : 10
Output : 1010

Input: 33
Output: 100001

Algorithm:

    Store the remainder when the number is divided by 2 in an array.
    Divide the number by 2
    Repeat the above two steps until the number is greater than zero.
    Print the array in reverse order now.


```cpp

#include <iostream> 
using namespace std; 

// function to convert decimal to binary 
void decToBinary(int n) 
{ 
	// array to store binary number 
	int binaryNum[32]; 

	// counter for binary array 
	int i = 0; 
	while (n > 0) { 

		// storing remainder in binary array 
		binaryNum[i] = n % 2; 
		n = n / 2; 
		i++; 
	} 

	// printing binary array in reverse order 
	for (int j = i - 1; j >= 0; j--) 
		cout << binaryNum[j]; 
} 

// Driver program to test above function 
int main() 
{ 
	int n = 17; 
	decToBinary(n); 
	return 0; 
} 


```


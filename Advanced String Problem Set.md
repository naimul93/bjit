#Palindromes

1. Print all palindromic partitions of a string
2. Minimum number of Appends needed to make a string palindrome
3. Find longest palindrome formed by removing or shuffling chars from string
4. Number of strings of length N with no palindromic sub string
5. Minimum equal palindromic cuts with rearrangements allowed

#Anagrams

1. https://www.hackerearth.com/problem/algorithm/anagrams/
2. https://www.hackerearth.com/problem/algorithm/anagrams-643/
3. https://www.hackerearth.com/problem/algorithm/anagrams-revisited-26/
4. Find Anagram Mappings
5. Group Anagrams

#Parenthesis

1. Minimum Add to Make Parentheses Valid
2. Generate Parentheses
3. Score of Parentheses
4. Different Ways to Add Parentheses 
5. Remove Outermost Parentheses 


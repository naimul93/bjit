2. Gotta Catch 'Em All
```cpp
#include <iostream>
#include<unordered_map>
using namespace std;

int main() {
	int t,n;
	cin>>t>>n;
	while(t--) {
		unordered_map<int,int> umap;
		for(int i=1;i<=n;i++) {
			int strength=0;
			for(int j=1;j<=n;j++) {
				if(i%j==0)
					strength++;
			}
			umap.insert({i,strength});
		}
		int v,val;
		cin>>v;
		val= umap[v];
		int count=0;
		for(auto x: umap) {
			if(val>x.second)
				count++;
		}
		cout<<count<<endl;
	}
	return 0;
}
```

4. GCD and Fibonacci Numbers
```cpp
#include <iostream>
using namespace std;

int fib(int n) {
	if(n<=1)
		return n;
	return fib(n-1)+fib(n-2);
}

int GCD(int a, int b) {
	if(a==0)
		return b;
	return GCD(b%a, a);
}

int main() {
	int m,n;
	cin>>m>>n;
	cout<<GCD(fib(m),fib(n));
	return 0;
}
```

5. Calculate the Power

```cpp
#include <iostream>
#include<cmath>
#define ll long long
using namespace std;

ll mod= 1000000000+7;

int main() {
	ll a,b,result;
	cin>>a>>b;
	result= pow(a,b);
	result= result%mod;
	cout<<result;
	return 0;
}
```
06. MikeAndGCDIssues
```java
package com.ignite.abdullah.math;

import java.util.Scanner;
//5
//2 3 4 9 17
//3 4 1 2 -1 
public class _06MikeAndGCDIssues {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int[] arr = new int[n];
		int[] res = new int[n];

		for (int i = 0; i < n; i++) 
			arr[i] = s.nextInt();
		

		for (int i = 0; i < n; i++) {
			int min = Integer.MAX_VALUE;

			for (int j = 0; j < n; j++) {
				if ( i!= j && gcd(arr[i], arr[j]) > 1 && Math.abs(i - j)< min) {
					min = Math.abs(i - j);
					res[i] = j+1;
				}
			}
			if (res[i] == 0)
				res[i] = res[i]-1;

		}
		
		for (int i : res) {
			System.out.print(i+ " ");
		}

	}

	static int gcd(int a, int b) {
		if (b == 0)
			return a;
		return gcd(b, a % b);
	}
}

```

07. EfficientProgramToPrintAllPrimeFactorsOfAGivenNumber

```java
package com.ignite.abdullah.math;

public class _07_1EfficientProgramToPrintAllPrimeFactorsOfAGivenNumber {

	public static void primeFactors(int n) {
		while (n % 2 == 0) {
			System.out.print(2 + " ");
			n /= 2;
		}

		for (int i = 3; i <= Math.sqrt(n); i += 2) {
			while (n % i == 0) {
				System.out.print(i + " ");
				n /= i;
			}
		}

		if (n > 2)
			System.out.print(n);
	}

	public static void main(String[] args) {
		int n = 315;
		primeFactors(n);
	}
}

```

09. HighestPrime

```java
package com.ignite.abdullah.math;

public class _09HighestPrime {
	static long maxPrimeFactors(long n) {
		long maxPrime = -1;

		while (n % 2 == 0) {
			maxPrime = 2;
//			n >>= 1;// equivalent to n /= 2 
			n /= 2 ;
		}

		for (int i = 3; i <= Math.sqrt(n); i += 2) {
			while (n % i == 0) {
				maxPrime = i;
				n = n / i;
			}
		}

		if (n > 2)
			maxPrime = n;

		return maxPrime;
	}

	public static void main(String[] args) {
		Long n = 15l;
		System.out.println(maxPrimeFactors(n));

		n = 25698751364526l;
		System.out.println(maxPrimeFactors(n));
	}
}

```

10. PrimeString 

```java
package com.ignite.abdullah.math;

public class _10PrimeString {
	static boolean isPrimeString(String str) {
		int len = str.length(), n = 0;
		for (int i = 0; i < len; i++)
			n += (int) str.charAt(i);

		System.out.println(n);

		return isPrime(2, (int) Math.sqrt(n), n);
	}

	static boolean isPrime(int i, int rt, int n) {

		if (n < 2)
			return false;
		if (n % i == 0)
			return false;
		if (i > rt)
			return true;

		return isPrime(i + 1, rt, n);

	}

	public static void main(String[] args) {
		String str = "geeksforgeeks";
		System.out.println(isPrimeString(str));
	}
}

```

11. Least Prime Factor
```cpp
#include <iostream>
using namespace std;

bool isPrime(int n) {
	int flag=1;
	for(int j=2;j<=n/2;j++) {
		if(n%j==0) {
			flag=0;
			break;
		}
	}
	if(flag==1)
		return true;
	else
		return false;
}

int main() {
	int m;
	cin>>m;
	for(int i=1;i<=m;i++) {
		if(i%2==0) 
			cout<<2<<endl;
		else if(isPrime(i))
			cout<<i<<endl;
		else {
			for(int j=2;j<=i/2;j++) {
				if(i%j==0 && isPrime(j)) {
					cout<<j<<endl;
					break;
				}
			}
		}
	}
	return 0;
}
```


13. GCD_LCM_AndDistributiveProperty

```java
package com.ignite.abdullah.math;

public class _13_1GCD_LCM_AndDistributiveProperty {

	static int gcd(int i, int j) {
		if (j == 0)
			return i;
		return gcd(j , i%j);
	}
	
	static int lcm(int i, int j) {
		return (i*j)/gcd(i,j);
	}

	// Returns value of GCD(LCM(x,y), LCM(x,z))
	static int findValue(int x, int y, int z) {
		int lcmxy = lcm(x, y);
		int lcmxz = lcm(x, z);
		return gcd(lcmxy, lcmxz);
	}

	public static void main(String[] args) {
		int x = 30, y = 40, z = 400;
		System.out.print(findValue(x, y, z));
	}
}

```


14. k-th prime factor of a given number

```cpp
# include<bits/stdc++.h> 
using namespace std; 
int kPrimeFactor(int n, int k) 
{ 
	while (n%2 == 0) 
	{ 
		k--; 
		n = n/2; 
		if (k == 0) 
		return 2; 
	} 
	for (int i = 3; i <= sqrt(n); i = i+2) 
	{ 
		while (n%i == 0) 
		{ 
			if (k == 1) 
			return i; 

			k--; 
			n = n/i; 
		} 
	}
	
	if (n > 2 && k == 1) 
		return n; 

	return -1; 
} 

int main() 
{ 
	int n = 12, k = 3; 
	cout << kPrimeFactor(n, k) << endl; 
	n = 14, k = 3; 
	cout << kPrimeFactor(n, k) << endl; 
	return 0; 
} 
```

15. Integer to English Words
```cpp
#include <iostream> 
using namespace std; 


string one[] = { "", "one ", "two ", "three ", "four ", 
				"five ", "six ", "seven ", "eight ", 
				"nine ", "ten ", "eleven ", "twelve ", 
				"thirteen ", "fourteen ", "fifteen ", 
				"sixteen ", "seventeen ", "eighteen ", 
				"nineteen "
			}; 


string ten[] = { "", "", "twenty ", "thirty ", "forty ", 
				"fifty ", "sixty ", "seventy ", "eighty ", 
				"ninety "
			}; 


string numToWords(int n, string s) 
{ 
	string str = ""; 
	
	if (n > 19) 
		str += ten[n / 10] + one[n % 10]; 
	else
		str += one[n]; 

	
	if (n) 
		str += s; 

	return str; 
} 


string convertToWords(long n) 
{ 
	
	string out; 

	
	out += numToWords((n / 10000000), "crore "); 


	out += numToWords(((n / 100000) % 100), "lakh "); 

	
	out += numToWords(((n / 1000) % 100), "thousand "); 

	
	out += numToWords(((n / 100) % 10), "hundred "); 

	if (n > 100 && n % 100) 
		out += "and "; 

	
	out += numToWords((n % 100), ""); 

	return out; 
} 


int main() 
{ 
	
	long n = 438237764; 

	
	cout << convertToWords(n) << endl; 

	return 0; 
} 
```

16. AdditionOfTwoMatrices 
```java
package com.ignite.abdullah.math;

public class _16AdditionOfTwoMatrices {

	static final int N = 3;

	static void add(int A[][], int B[][], int C[][]) {
		int i, j;
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
				C[i][j] = A[i][j] + B[i][j];
	}

	public static void main(String[] args) {
		int[][] A = { 
				{ 1, 1, 1 }, 
				{ 2, 2, 2 }, 
				{ 3, 3, 3 }
				};

		int[][] B = { 
				{ 1, 1, 1 }, 
				{ 2, 2, 2 }, 
				{ 3, 3, 3 }
				};

		int[][] C = new int[N][N];
		int i, j;
		add(A, B, C);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++)
				System.out.print(C[i][j] + " ");
			System.out.print("\n");
		}
	}

}

```

17. SubtractionOfMatrices

```java
package com.ignite.abdullah.math;

public class _17SubtractionOfMatrices {

	static final int N = 3;

	static void multiply(int A[][], int B[][], int C[][]) {
		int i, j;
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
				C[i][j] = A[i][j] - B[i][j];
	}

	public static void main(String[] args) {
		int[][] A = { 
				{ 1, 1, 1 }, 
				{ 2, 4, 2 }, 
				{ 3, 3, 3 } 
				};

		int[][] B = {
				{ 2, 1, 1 }, 
				{ 2, 2, 2 }, 
				{ 3, 3, 3 } 
				};

		int[][] C = new int[N][N];

		int i, j;
		multiply(A, B, C);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++)
				System.out.print(C[i][j] + " ");
			System.out.print("\n");
		}
	}

}

```


18. CountNumberOfDigitsAfter_Dot_InFloatingPointNumbers

```java
package com.ignite.abdullah.math;

public class _18CountNumberOfDigitsAfter_Dot_InFloatingPointNumbers {
	static boolean is_integer(double k) {
		return Math.floor(k) == k;
	}

	public static void main(String[] args) {
		double num = 3.554 ;
		int counter = 0;
		while (!is_integer(num)) {
			counter++;
			num = num * 10;
		}
		System.out.println(counter);
	}
}

```

19. BinaryToDecimal

```java
package com.ignite.abdullah.math;

public class _19BinaryToDecimal {
	static int binaryToDecimal(int n) {
		int dec = 0;

		// Initializing base value to 1, i.e 2^0
		int base = 1;
		while (n != 0) {
			int digit = n % 10;
			n /= 10;

			dec += digit * base;
			base = base * 2;
		}
		return dec;
	}

	public static void main(String[] args) {
		int num = 111;
		System.out.println(binaryToDecimal(num));
	}
}

```

20. DecimalToBinary
```java
package com.ignite.abdullah.math;

public class _20DecimalToBinary {

	static void decToBinary(int n) {
		int[] binaryNum = new int[32];
		int i = 0;
		while (n > 0) {
			binaryNum[i] = n % 2;
			n /= 2;
			i++;
		}

		for (int j = i - 1; j >= 0; j--)
			System.out.print(binaryNum[j]);
	}

	public static void main(String[] args) {
		int n = 7;
		decToBinary(n);
	}
}

```

21. Armstrong
```java
package com.ignite.abdullah.math;

public class _21Armstrong {
	public static void main(String[] args) {
		int c = 0, a, temp;
		int n = 153;// It is the number to check armstrong
		temp = n;
		while (n > 0) {
			a = n % 10;
			n /= 10;
			c = c + (a * a * a);
		}
			System.out.println(temp == c);
	}
}

```

22. BinomialCoeff
```java
package com.ignite.abdullah.math;

public class _22BinomialCoeff {
	static void printPascal(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++)
				System.out.print(binomialCoeff(i, j) + " ");
			System.out.println();
		}
	}
	static int binomialCoeff(int n, int k) {
		int res = 1;
		if (k > n - k)
			k = n - k;
		for (int i = 0; i < k; ++i) {
			res *= (n - i);
			res /= (i + 1);
		}
		return res;
	}
	
	public static void main(String args[]) {
		int n = 7;
		printPascal(n);
	}
}
```



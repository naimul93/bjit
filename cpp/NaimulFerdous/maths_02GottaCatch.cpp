// 2. Gotta Catch 'Em All

#include <iostream>
#include<unordered_map>
using namespace std;

int main() {
	int t,n;
	cin>>t>>n;
	while(t--) {
		unordered_map<int,int> umap;
		for(int i=1;i<=n;i++) {
			int strength=0;
			for(int j=1;j<=n;j++) {
				if(i%j==0)
					strength++;
			}
			umap.insert({i,strength});
		}
		int v,val;
		cin>>v;
		val= umap[v];
		int count=0;
		for(auto x: umap) {
			if(val>x.second)
				count++;
		}
		cout<<count<<endl;
	}
	return 0;
}
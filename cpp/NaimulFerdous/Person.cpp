#include <iostream>
#include<map>
#include<utility>
#include<list>
using namespace std;

class Person {       
  public:          
    string name;  
    string email;
    string phone;
    int year;     
    Person(string x, string y, string z) { 
      name = x;
      email = y;
      phone = z;
    }
    string toString() {
    	return name+" "+email+" "+phone;
    }
};

int main() {
	
	Person p1("Abdullah", "cm@gmail.com", "0171");
	Person p2("Naimul", "naim@gmail.com", "0191");
	
	list<Person> myList;
	myList.push_back(p1);
	myList.push_back(p2);
	
	//string
	map<string, string> strMap;
	strMap.insert({"abdullah", "khan"});
	strMap.insert({"naimul", "haque"});
	for(auto x: strMap) {
		cout<<x.second<<endl;
	}
	
	//object
	map<string, Person> objMap;
	objMap.emplace(make_pair("abdullah", p1));
	objMap.emplace(make_pair("naimul", p2));
	for(auto x: objMap) {
		cout<<x.first<<": "<<x.second.toString()<<endl;
	}
	
	//list
	map<string, list<Person>> lstMap;
	lstMap.emplace(make_pair("abdullah", myList));
	list<Person> resultList;
	for(auto x: lstMap) {
		resultList=x.second;
	}
	for(auto p: resultList)
		cout<<p.toString()<<endl;
	return 0;
}
// 29. Find local minima in array

#include <iostream>
using namespace std;
// int [] arr = {10, 5, 3, 6, 13, 16, 7};
int localminima(int arr[],int n) {
	if(n==1) 
		return arr[0];
	for(int i=0;i<n;i++) {
		if(i==0 && arr[i]<arr[i+1])
			return arr[i];
		if(i==n-1 && arr[i]<arr[i-1])
			return arr[i];
		else {
			if(arr[i]<arr[i-1] && arr[i]<arr[i+1])
				return arr[i];
		}
	}
}

int main() {
	int arr[] = {8,6};
	int size= sizeof(arr)/sizeof(arr[0]);
	cout<<localminima(arr,size);
	return 0;
}
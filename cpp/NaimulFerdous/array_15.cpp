// 15. Have the function PentagonalNumber(num)

#include <iostream>
#include <string>
using namespace std;

int PentagonalNumber(int num) { 

  return 1 + 5*(num * (num - 1) / 2);
            
}

int main() { 
  
  cout << PentagonalNumber(5);
  return 0;
    
} 


  
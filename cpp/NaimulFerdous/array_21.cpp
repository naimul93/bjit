// 21. Rotate Array
#include <iostream>
using namespace std;

void rotateKTimes(int arr[], int n, int k) {
	for(int i=0;i<k;i++) {
		int temp= arr[n-1];
		for(int i=n-1;i>0;i--) {
			arr[i]=arr[i-1];
		}
		arr[0]=temp;
	}
}

int main() {
	int arr[]= {-1,-100,3,99};
	int size= sizeof(arr)/sizeof(arr[0]);
	rotateKTimes(arr,size,2);
	
	for(int i=0;i<size;i++) {
		cout<<arr[i]<<" ";
	}
	return 0;
}
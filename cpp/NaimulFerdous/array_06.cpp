// 06. Count number of digits after `.` in floating point numbers?
// e.g. if given 3.554 output=3

// for 43.000 output=0.

#include <iostream>
#include <cmath>
#include <math.h>
using namespace std;

bool is_integer(double k)
{
  return floor(k) == k;
}

// bool IsInt(double n)
// {
//     return !(n - trunc(n));
// }

int main() {
	double num;
	cin>>num;
	int counter=0;
	while(!is_integer(num)) {
		counter++;
		num= num*10;
	}
	cout<<"Number of digits after decimal point: "<<counter;
	return 0;
}
// 10. Funny String

#include <bits/stdc++.h>
#include<vector>
using namespace std;

// Complete the funnyString function below.
string funnyString(string s) {
    vector<int> sdiff;
    vector<int> rdiff;
    for(int i=0;i<s.length()-1;i++) {
        sdiff.push_back(abs(s[i+1]-s[i]));
    }
    for(int i=s.length()-1;i>0;i--) {
        rdiff.push_back(abs(s[i-1]-s[i]));
    }
    for(int i=0;i<sdiff.size();i++) {
        if(sdiff[i]!=rdiff[i])
            return "Not Funny";
    }
    return "Funny";

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        string result = funnyString(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}

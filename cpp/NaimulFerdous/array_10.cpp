#include <iostream>
using namespace std;

bool isPrime(int num) {
	int flag=1;
	for(int i =2;i<=num/2;i++) {
		if(num%i==0) {
			flag=0;
			break;
		}
	}
	if(flag==1)
		return true;
	else
		return false;
}

int main() {
	int num;
	cin>>num;
	isPrime(num)?cout<<num<<" is prime" : cout<<num<<" is not prime";
	return 0;
}
// 37. find the minimum distance between tow numbers

#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int distance(int arr[], int n, int x, int y) {
	int minDistance=INT_MAX;
	for(int i=0;i<n-1;i++) {
		for(int j=i+1;j<n;j++) {
			if((x==arr[i] && y==arr[j]) || (y==arr[i] && x==arr[j]) && abs(i-j)<minDistance)
				minDistance= abs(i-j);
		}
	}
	return minDistance;
}

int main() {
	
	int arr[] = {3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3};
	int size= sizeof(arr)/sizeof(arr[0]);
	int x=3;
	int y=6;
	cout<<distance(arr,size,x,y);
	
	return 0;
}
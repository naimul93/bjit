// 33. find all elements in array which have at-least two greater elements

#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;

void swap(int *x, int *y) {
	int temp= *x;
	*x= *y;
	*y= temp;
}

void sort(int a[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(a[j]>a[j+1])
				swap(&a[j], &a[j+1]);
		}
	}
}

int main() {
	int arr[]= {7, 12, 9, 15, 19, 32, 56, 70};
	int n= sizeof(arr)/sizeof(arr[0]);
	sort(arr,n);
	for(int i=0;i<n-2;i++) {
		cout<<arr[i]<<" ";
	}
	
	return 0;
}

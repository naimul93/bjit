// 18. Game of Thrones - I

#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

// Complete the gameOfThrones function below.
string gameOfThrones(string s) {
    int size= s.length();
    unordered_map<char, int> umap;
    for(int i=0;i<size;i++){
        umap[s[i]]++;
    }
    int oddcount=0;
    for(auto x: umap){
        if(x.second%2==1)
            oddcount++;
    }
    if(size%2==1 && oddcount==1)
        return "YES";
    else if(size%2==0 && oddcount==0)
        return "YES";
    else
        return "NO";
    


}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = gameOfThrones(s);

    fout << result << "\n";

    fout.close();

    return 0;
}

// 4. GCD and Fibonacci Numbers

#include <iostream>
using namespace std;

int fib(int n) {
	if(n<=1)
		return n;
	return fib(n-1)+fib(n-2);
}

int GCD(int a, int b) {
	if(a==0)
		return b;
	return GCD(b%a, a);
}

int main() {
	int m,n;
	cin>>m>>n;
	cout<<GCD(fib(m),fib(n));
	return 0;
}
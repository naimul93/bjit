// 35. find the smallest and second smallest elements in an array

#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;

void swap(int *x, int *y) {
	int temp= *x;
	*x= *y;
	*y= temp;
}

void sort(int a[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(a[j]>a[j+1])
				swap(&a[j], &a[j+1]);
		}
	}
}

int main() {
	int arr[]= {2,6,-1,2,4,1,-6,5};
	int n= sizeof(arr)/sizeof(arr[0]);
	sort(arr,n);
	cout<<arr[0]<<" "<<arr[1];
	
	return 0;
}
// 22.Valid Parentheses

#include <iostream>
#include<bits/stdc++.h>
#include<unordered_map>
#define SIZE 101
using namespace std;

int top=-1;
char Stk[SIZE];

void push(char c) {
    if(top==SIZE-1) {
        cout<<"Overflow";
        return;
    }
    Stk[++top]=c;
}

void pop() {
    if(top==-1) {
        cout<<"Underflow";
        return;
    }
    top--;
}

bool isValid(string Parentheses) {
    string starts="[{(";
    string ends= "]})";
    unordered_map<char,char> umap;
    for(int i=0;i<starts.length();i++) {
        umap[starts[i]]=ends[i];
    }
    for(int i=0;i<Parentheses.length();i++) {
        for(auto x: umap) {
            if(Parentheses[i]==x.first) 
                push(Parentheses[i]);
            else if(Parentheses[i]==x.second && x.first==Stk[top])
                pop();
            else if(top==-1 && Parentheses[i]==x.second){
            	push(Parentheses[i]);
            	break;
            }
            	
        }
    }
    if(top==-1)
        return true;
    else
        return false;
}


int main() {
    isValid("[[[[[[[[[{{{{{{{(((Naimul)))}}}}}}}]]]]]]]]]")?cout<<"true":cout<<"false";
	return 0;
}
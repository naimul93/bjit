// 11. Least Prime Factor

#include <iostream>
using namespace std;

bool isPrime(int n) {
	int flag=1;
	for(int j=2;j<=n/2;j++) {
		if(n%j==0) {
			flag=0;
			break;
		}
	}
	if(flag==1)
		return true;
	else
		return false;
}

int main() {
	int m;
	cin>>m;
	for(int i=1;i<=m;i++) {
		if(i%2==0) 
			cout<<2<<endl;
		else if(isPrime(i))
			cout<<i<<endl;
		else {
			for(int j=2;j<=i/2;j++) {
				if(i%j==0 && isPrime(j)) {
					cout<<j<<endl;
					break;
				}
			}
		}
	}
	return 0;
}
// 20. String Construction
#include <bits/stdc++.h>
#include<set>
using namespace std;

// Complete the stringConstruction function below.
int stringConstruction(string s) {
    set<char> distincts;
    for(int i=0;i<s.length();i++) {
        distincts.insert(s[i]);
    }
    int counter=0;
    return distincts.size();

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        int result = stringConstruction(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}

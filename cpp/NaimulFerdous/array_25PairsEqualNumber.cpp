//  25. Find all pairs of elements from an array whose sum is equal to given number

#include <iostream>
#include<bits/stdc++.h>
#include<map>
using namespace std;

int main() {
	int array[]={-40, -5, 1, 3, 6, 7, 8, 20};
	int x= 15;
	int size= sizeof(array)/sizeof(array[0]);
	map<int,int> mymap;
	int pair[2];
	for(int i=0;i<size-1;i++) {
		for(int j=i+1;j<size;j++) {
			int sum= array[i]+array[j];
			if(sum==x) {
				pair[0]=array[i];
				pair[1]= array[j];
				mymap.insert({pair[0],pair[1]});
			}
				
		}
	}
	for(auto x: mymap)
		cout<<x.first<<", "<<x.second<<endl;
	
	return 0;
}
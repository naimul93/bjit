// 24. Sherlock and Anagrams

#include <bits/stdc++.h>
#include<map>
#include <algorithm>
#include<string>

using namespace std;

// bool anagram(string &s1, string &s2) {
//     sort(s1.begin(),s1.end());
//     sort(s2.begin(),s2.end());
//     if(s1.length()!=s2.length())
//         return false;
//     for(int i=0;i<s1.length();i++) {
//         if(s1[i]!=s2[i])
//             return false;
//     }
//     return true;
// }

// Complete the sherlockAndAnagrams function below.
int sherlockAndAnagrams(string s) {
    map<string,int> allSubs;
    int n= s.length();
    
    for(int i=0;i<n;i++) {
        for(int len=1;len<=n-i;len++) {
            string t=(s.substr(i,len));
            sort(t.begin(), t.end());
            allSubs[t]++;
        } 
    }
    long long count=0;
    for(auto x: allSubs) {
        count+=(long long)(x.second)*(x.second-1)/2;
    }
    return count;

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        int result = sherlockAndAnagrams(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}

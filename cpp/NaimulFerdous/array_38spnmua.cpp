// 38. find the smallest positive number missing from an unsorted array

#include <iostream>
#include<bits/stdc++.h>
#include<unordered_map>
using namespace std;

int main() {
	int arr[]= {1,2,3,4,6,9,11,15};
	int n= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> mmap;
	for(int i=0;i<n;i++) {
		mmap[arr[i]]++;
	}
	for(int i=0;i<n;i++) {
		if(mmap[i]==0) {
			cout<<i;
			break;
		}
		
	}
	
	
	return 0;
}
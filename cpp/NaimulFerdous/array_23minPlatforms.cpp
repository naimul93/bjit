// 23. Find the minimum number of platforms required for railway station

#include <iostream>
#include<vector>
using namespace std;

string split(string s) {
	string split="";
	for(int i=0;i<s.length();i++) {
		if(s[i]!=':') 
			split+=s[i];
	}
	return split;
}

int minPlatforms(string arriv[], string depart[], int size) {
	vector<int> arrival;
    vector<int> departure;
    for(int i=0;i<size;i++) {
    	arrival.push_back(stoi(split(arriv[i])));
    	departure.push_back(stoi(split(depart[i])));
    }
    int platform=1;
    for(int i=1;i<size;i++) {
    	int temp=platform;
    	int flag=0;
    	while(temp>0) {
    		if(arrival[i]<departure[i-temp])
    			flag=1;
    		else {
    			flag=0;
    			break;
    		}
    		temp--;
    	}
    	if(flag==1)
    		platform++;
    }
    return platform;
}

int main() {
	string arrival[] = {"9:00", "9:40", "9:50", "11:00", "15:00", "18:00"};
    string departure[] = {"9:10", "12:00", "11:20", "11:30", "19:00", "20:00"};
    int size= sizeof(arrival)/sizeof(arrival[0]);
    cout<<minPlatforms(arrival, departure, size);
	return 0;
}
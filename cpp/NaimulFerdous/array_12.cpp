// 12. Reverse the words of a sentence

#include <iostream>
#include<vector>
#define space ' '
using namespace std;

string reverseWords(string sentence) {
	vector<string> words;
	string word="";
	for(int i=0;i<=sentence.length();i++) {
		if(sentence[i]==space) {
			words.push_back(word);
			word="";
		}
		else if(i==sentence.length())
			words.push_back(word);
		else
			word+=sentence[i];
	}
	string result="";
	for(int i=0;i<words.size();i++) {
		if(i==words.size()-1)
			result=words[i]+result;
		else
			result=" "+words[i]+result;
	}
	
	return result;
}

int main() {
	cout<<reverseWords("Bangladesh and Dhaka");
	return 0;
}
// 02. Reverse the names and then sort them in lexicographical order.
//         Examples:
//         Input: Naimul Ferdous, Abdullah Khan, Zahid Ahsan
//         Output: Ahsan Zahid, Ferdous Naimul, Khan Abdullah  

#include <iostream>
#include<string>
#include<vector>
#include<algorithm>
#define space ' '
using namespace std;
 
string reverseName(string name) {
	vector<string> vec;
	string word="";
	for(int i=0;i<=name.length();i++) {
		if(name[i]==space) {
			vec.push_back(word);
			word="";
		}
		else if(i==name.length()) 
			vec.push_back(word);
		else
			word+=name[i];
	}
 
	return vec[1]+" "+vec[0];
}
 
int main() {
	string names[]={"Naimul Ferdous", "Abdullah Khan", "Bashir Ahmed"};
	int n= sizeof(names)/sizeof(names[0]);
	vector<string> result;
	for(int i=0;i<n;i++) {
		result.push_back(reverseName(names[i]));
	}
	sort(result.begin(),result.end());
	for(auto x: result) {
		cout<<x<<endl;
	}
	return 0;
}
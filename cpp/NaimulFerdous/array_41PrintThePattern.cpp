// 41. print the pattern using only a single loop: 98765432123456789

#include <iostream>
using namespace std;

void PrintPattern(int m) 
{ 
    if (m > 1) 
    { 
        cout << m << " "; 
        PrintPattern(m - 1); 
    } 
  
    cout << m << " "; 
    m += 1; 
} 
  
int main() 
{ 
     int n = 9; 
     PrintPattern(n); 
    return 0; 
} 
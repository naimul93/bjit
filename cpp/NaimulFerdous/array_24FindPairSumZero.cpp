// 24. Find a Pair Whose Sum is Closest to zero in Array

#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int main() {
	int array[]={1,3,-5,7,8,20,-40,6};
	int size= sizeof(array)/sizeof(array[0]);
	int min= INT_MAX;
	int pair[2];
	for(int i=0;i<size;i++) {
		for(int j=0;j<size;j++) {
			int sum= array[i]+array[j];
			int diff= abs(sum);
			if(diff<min) {
				min=diff;
				pair[0]=array[i];
				pair[1]= array[j];
			}
				
		}
	}
	cout<<pair[0]<<" and "<<pair[1];
	
	return 0;
}
// 28. Given an array containing zeroes, ones and twos only. Write a function to sort the given array in O(n) time complexity.

#include <iostream>
using namespace std;

int main() {
	int arr[]={1, 2, 2, 0, 0, 1, 2, 2, 1};
	int size= sizeof(arr)/sizeof(arr[0]);
	int count0, count1, count2;
	count0=0;
	count1=0;
	count2=0;
	for(int i=0;i<size;i++) {
		if(arr[i]==0)
			count0++;
		if(arr[i]==1)
			count1++;
		if(arr[i]==2)
			count2++;
	}
	for(int i=0;i<count0;i++) {
		arr[i]=0;
	}
	for(int i=count0;i<(count0+count1);i++) {
		arr[i]=1;
	}
	for(int i=(count0+count1);i<size;i++) {
		arr[i]=2;
	}
	for(int j=0;j<size;j++) {
		cout<<arr[j]<<" ";
	}
	return 0;
}
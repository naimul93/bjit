// 11. Print All Distinct Elements of a given integer array

#include <iostream>
#include<unordered_map>
using namespace std;

int main() {
	int arr[]={4,3,6,3,5,5,8,9,7,6};
	int size= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> elements;
	for(int i=0;i<size;i++) {
		elements[arr[i]]++;
	}
	for(auto x: elements) 
		cout<<x.first<<" ";
	return 0;
}
// 27. Separate odd and even numbers in an array

#include <iostream>
using namespace std;
 
void swap(int *x, int *y) {
	int temp= *x;
	*x=*y;
	*y=temp;
}
 
int main() {
	int arr[] = {12, 17, 70, 15, 22, 65, 21, 90};
	int size= sizeof(arr)/sizeof(arr[0]);
	int j=-1;
	for(int i=0;i<size;i++) {
		if(arr[i]%2==0) {
			j++;
			swap(&arr[i], &arr[j]);
		}
	}
	for(int j=0;j<size;j++) {
		cout<<arr[j]<<" ";
	}
	return 0;
}
// 14. Have the function MaximalSquare(strArr)

#include<bits/stdc++.h>
#include <iostream>
#include<algorithm>
#include<string>
using namespace std;

int MaximalSquare(string strArr[], int inpArrSize) { 
	
	
	int eachStrSize= strArr[0].length();
	int mainMatrix[inpArrSize][eachStrSize];
    for(int i=0;i<inpArrSize;i++) {
        for(int j=0;j<eachStrSize;j++) {
            mainMatrix[i][j]=(strArr[i][j]-'0');
        }
    }

    int subMatrix[inpArrSize][eachStrSize];

    for(int i=0;i<eachStrSize;i++) {
        subMatrix[0][i]=mainMatrix[0][i];
    }

    for(int j=0;j<inpArrSize;j++) {
        subMatrix[j][0]=mainMatrix[j][0];
    }

    for(int i=1;i<inpArrSize;i++) {
        for(int j=1;j<eachStrSize;j++) {
            if(mainMatrix[i][j]==1)
                subMatrix[i][j]=min(subMatrix[i-1][j-1], min(subMatrix[i][j-1], subMatrix[i-1][j]))+1;
            else
                subMatrix[i][j]=0;
        }
    }
    int max=1;

    for(int i=1;i<inpArrSize;i++) {
        for(int j=1;j<eachStrSize;j++) {
            if(subMatrix[i][j]>max)
                max=subMatrix[i][j];
        }
    }
  
	return max*max; 
            
}

int main() {
	string strArr[]={
					"01101",  
                    "11010",  
                    "01110",  
                    "11110",  
                    "11111",  
                    "00000"
					};
	int inpArrSize= sizeof(strArr)/sizeof(strArr[0]);
    cout<<"Largest square submatrix of the given matrix: "<<MaximalSquare(strArr,inpArrSize);
	return 0;
}
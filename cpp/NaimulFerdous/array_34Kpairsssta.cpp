// 34. find k pairs with smallest sums in two arrays

#include <iostream>
#include<bits/stdc++.h>
#include<vector>
#include<map>
using namespace std;

int main() {
	int nums1[]= {1,2};
	int nums2[]= {3};
	int k=3;
	int n1= sizeof(nums1)/sizeof(nums1[0]);
	int n2= sizeof(nums2)/sizeof(nums2[0]);
	map<int, vector<int>> mymap;
	vector<int> myvec;
	for(int i=0;i<n1;i++) {
		for(int j=0;j<n2;j++) {
			vector<int> temp;
			int sum= nums1[i]+nums2[j];
			temp.push_back(nums1[i]);
			temp.push_back(nums2[j]);
			mymap.insert({sum, temp});
		}
	}
	
	for(auto x: mymap)
		myvec.push_back(x.first);
		
	sort(myvec.begin(),myvec.end());
	map<int,int> tempo;
	for(int i=0;i<k;i++) {
		cout<<mymap[myvec[i]][0]<<", "<<mymap[myvec[i]][1]<<endl;
		
	}
	
	return 0;
}
// 40. find the largest pair sum in an unsorted array

#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;
 
 
int main() {
	int arr[]= {12, 34, 10, 6, 40};
	int n= sizeof(arr)/sizeof(arr[0]);
	int pair[2];
	int largestSum=INT_MIN;
	for(int i=0;i<n-1;i++) {
		for(int j=i+1;j<n;j++) {
			int sum= arr[i]+arr[j];
			if(sum>largestSum) {
				largestSum=sum;
				pair[0]= arr[i];
				pair[1]= arr[j];
			}
 
		}
	}
	cout<<"Largest sum: "<<largestSum<<endl;
	cout<<"Pair: "<<pair[0]<<", "<<pair[1];
 
	return 0;
}
// 17. Making Anagrams

#include <bits/stdc++.h>
#include <map>
using namespace std;

// Complete the makingAnagrams function below.
int makingAnagrams(string s1, string s2) {
    map<char, int> map1;
    map<char, int> map2;
    for(int i=0;i<s1.length();i++) {
        map1[s1[i]]++;
    }
    for(int j=0;j<s2.length();j++) {
        map2[s2[j]]++;
    }
    int count=0;
    for(auto x: map1) {
        for(auto y: map2) {
            if(x.first==y.first) {
                count+=2*min(x.second, y.second);
            }
        }
    }
    return (s1.length()+s2.length())-count;
    
    


}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s1;
    getline(cin, s1);

    string s2;
    getline(cin, s2);

    int result = makingAnagrams(s1, s2);

    fout << result << "\n";

    fout.close();

    return 0;
}

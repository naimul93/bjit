// 30. Sliding window maximum in java

#include <iostream>
#include<vector>
#include<bits/stdc++.h>
using namespace std;



int main() {
	int arr[] = {2,6,-1,2,4,1,-6,5};
	int k=3;
	int n= sizeof(arr)/sizeof(arr[0]);
	vector<int> vec;
	for(int i=0;i<=n-k;i++) {
		int max= INT_MIN;
		for(int j=0;j<k;j++) {
			if(arr[i+j]>max)
				max= arr[i+j];
		}
		vec.push_back(max);
	}
	for(auto x: vec) 
		cout<<x<<" ";
	return 0;
}
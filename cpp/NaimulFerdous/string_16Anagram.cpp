// 16. Anagram

#include <iostream>
#include<algorithm>
using namespace std;

bool anagram(string &s1, string &s2) {
	sort(s1.begin(),s1.end());
	sort(s2.begin(),s2.end());
	if(s1.length()!=s2.length())
		return false;
	for(int i=0;i<s1.length();i++) {
		if(s1[i]!=s2[i])
			return false;
	}
	return true;
}

int main() {
	string s1="Naimul";
	string s2="mulNai";
	anagram(s1,s2)?cout<<"Anagram":cout<<"Not anagram";
	
	return 0;
}
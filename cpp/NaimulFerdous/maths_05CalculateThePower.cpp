// 5. Calculate the Power
#include <iostream>
#include<cmath>
#define ll long long
using namespace std;

ll mod= 1000000000+7;

int main() {
	ll a,b,result;
	cin>>a>>b;
	result= pow(a,b);
	result= result%mod;
	cout<<result;
	return 0;
}
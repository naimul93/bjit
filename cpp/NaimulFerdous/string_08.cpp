// 08. Weighted Uniform Strings

#include <bits/stdc++.h>
#include<unordered_map>
#include<vector>

using namespace std;

// Complete the weightedUniformStrings function below.
vector<string> weightedUniformStrings(string s, vector<int> queries) {
    string alphabets= "abcdefghijklmnopqrstuvwxyz";
    unordered_map<char,int> weights;
    for(int i=0;i<alphabets.length();i++) {
        weights[alphabets[i]]=i+1;
    }
    vector<int> subsWeights;
    vector<string> result;

    int mul=1;
    for(int i=0;i<s.length();i++) {
        if(s[i]==s[i-1]) 
            mul++;
        else
            mul=1;
        cout<<mul;
        subsWeights.push_back(mul*weights[s[i]]);
    }

    for(auto x: queries) {
        int flag=0;
        for(auto y: subsWeights) {
            if(x==y){
                flag=1;
                break;
            }
        }
        if(flag==1)
            result.push_back("Yes");
        else
            result.push_back("No");
    }

    return result;

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    int queries_count;
    cin >> queries_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<int> queries(queries_count);

    for (int i = 0; i < queries_count; i++) {
        int queries_item;
        cin >> queries_item;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        queries[i] = queries_item;
    }

    vector<string> result = weightedUniformStrings(s, queries);

    for (int i = 0; i < result.size(); i++) {
        fout << result[i];

        if (i != result.size() - 1) {
            fout << "\n";
        }
    }

    fout << "\n";

    fout.close();

    return 0;
}

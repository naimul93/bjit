// 08. Write program to find the years from a sentence
// Example:
// Bangladesh became independent in 1971 and 1948.
// Here 1971 and 1948 is the year.

#include <iostream>
#include <vector>
#define space ' '
using namespace std;

bool isdig(char c) {
	if(c>='0' && c<='9') 
		return true;
	return false;
}

bool isYear(string word) {
	if(word.length()!=4)
		return false;
	for(int i=0;i<word.length();i++) {
		if(!isdig(word[i]))
			return false;
	}
	return true;
}

vector<string> words(string sentence) {
	vector<string> words;
	string word="";
	cout<<"Years are: "<<endl;
	for(int i=0;i<=sentence.length();i++) {
		if(sentence[i]==space || i== sentence.length()) {
			words.push_back(word);
			word="";
		}
		else
			word+=sentence[i];
	}
	return words;
}

int main() {
	string sentence= "Bangladesh 1948 29 th became independent in 1971 and";
	for(auto word: words(sentence)) {
		if(isYear(word))
			cout<<word<<endl;
	}
	return 0;
}
// 07. program to count the occurrences of each character
// Write a program which prints number of occurrences of each characters and also it should not print repeatedly occurrences of duplicate characters as given in the example:

// Examples:

// Input : geeksforgeeks
// Output :
// Number of Occurrence of g is:2
// Number of Occurrence of e is:4
// Number of Occurrence of k is:2
// Number of Occurrence of s is:2
// Number of Occurrence of f is:1
// Number of Occurrence of o is:1
// Number of Occurrence of r is:1


#include <iostream>
#include <unordered_map>
#include <string>
using namespace std;

int main() {
	string str;
	cin>>str;
	unordered_map<char, int> omap;
	for(int i=0;i<str.length();i++) {
		omap[str[i]]++;
	}
	for(auto x : omap) {
		cout<<x.first<<": "<<x.second<<endl;
	}
	return 0;
}
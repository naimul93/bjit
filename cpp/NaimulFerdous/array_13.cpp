// 13. Find the Missing Number

#include <iostream>
#include<unordered_map>
using namespace std;

void swap(int *x, int* y) {
	int temp= *x;
	*x=*y;
	*y=temp;
}

void sort(int arr[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(arr[j]>arr[j+1])
				swap(&arr[j], &arr[j+1]);
		}
	}
}

int main() {
	int arr[] = {1, 2, 5, 6, 3, 7, 8};
	int size= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> umap;
	sort(arr,size);
	int max=arr[size-1];
	for(int i=0;i<size;i++) {
		umap[arr[i]]++;
	}
	for(int i=1;i<=max;i++) {
		if(umap[i]==0)
			cout<<i;
	}
	return 0;
}
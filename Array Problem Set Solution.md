# 01 MaximumProduct

        Given an array with both +ive and -ive integers, return a pair with highest product.
        Examples :

        Input: arr[] = {1, 4, 3, 6, 7, 0}  
        Output: {6,7}  

        Input: arr[] = {-1, -3, -4, 2, 0, -5} 
        Output: {-4,-5}  

```java
package com.ignite.abdullah.array;

public class _01MaximumProduct {
	public static void main(String[] args) {
		int[] arr = { 5, 3, 6, 9, 8 };

		int max = 1;

		for (int i = 0; i < arr.length; i++) {

			for (int j = i + 1; j < arr.length; j++) {
				int temp = arr[i] * arr[j];

				if (max < temp)
					max = temp;
			}
		}
		System.out.println("max value is : " + max);
	}
}

```
# 02 Reverse the names and then sort them in lexicographical order.

        Examples:
        Input: Naimul Ferdous, Abdullah Khan, Zahid Ahsan
        Output: Ahsan Zahid, Ferdous Naimul, Khan Abdullah  

```c++
#include <iostream>
#include<string>
#include<vector>
#include<algorithm>
#define space ' '
using namespace std;
 
string reverseName(string name) {
	vector<string> vec;
	string word="";
	for(int i=0;i<=name.length();i++) {
		if(name[i]==space) {
			vec.push_back(word);
			word="";
		}
		else if(i==name.length()) 
			vec.push_back(word);
		else
			word+=name[i];
	}
 
	return vec[1]+" "+vec[0];
}
 
int main() {
	string names[]={"Naimul Ferdous", "Abdullah Khan", "Bashir Ahmed"};
	int n= sizeof(names)/sizeof(names[0]);
	vector<string> result;
	for(int i=0;i<n;i++) {
		result.push_back(reverseName(names[i]));
	}
	sort(result.begin(),result.end());
	for(auto x: result) {
		cout<<x<<endl;
	}
	return 0;
}
```

# 03 EquilibriumIndex

        Equilibrium index of an array is an index such that the sum of elements at lower indexes is equal to the sum of elements at higher indexes. For example, in an array A:

        Example :

        Input: A[] = {-7, 1, 5, 2, -4, 3, 0}
        Output: 3
        3 is an equilibrium index, because:
        A[0] + A[1] + A[2] = A[4] + A[5] + A[6]

        Input: A[] = {1, 2, 3}
        Output: -1

```java
package com.ignite.abdullah.array;

public class _03EquilibriumIndex {
	static int equilibrium(int arr[], int n) {
		int i, j;
		int leftsum, rightsum;

		for (i = 0; i < n; ++i) {

			leftsum = 0;
			for (j = 0; j < i; j++)
				leftsum += arr[j];

			rightsum = 0;
			for (j = i + 1; j < n; j++)
				rightsum += arr[j];
			if (leftsum == rightsum)
				return i;
		}

		return -1;
	}

	public static void main(String[] args) {
		int arr[] = { -7, 1, 5, 2, -4, 3, 0 };
		System.out.println(equilibrium(arr, arr.length));
	}
}

```

# 04 Palindrome

        Given a string, write a recursive function that check if the given string is palindrome, else not palindrome.

        Examples :

        Input : malayalam
        Output : Yes
        Reverse of malayalam is also
        malayalam.

        Input : max
        Output : No
        Reverse of max is not max.

```java
package com.ignite.abdullah.array;

public class _04Palindrome {
	static boolean flag = false;

	public static void main(String[] args) {
		String arr = "ABCDDCBA";
		char[] arr2 = arr.toCharArray();
		recurs(arr2, 0, arr2.length - 1);
	}

	private static void recurs(char[] arr2, int start, int end) {
		System.out.println("Start : " + start + " End : " + end);
		if (start <= end) {
			if (start == end) {
				System.out.println(flag);
			} else if (start == end - 1) {
				System.out.println(flag);
			} else if (arr2[start] == arr2[end]) {
				flag = true;
				recurs(arr2, ++start, --end);
			} else {
				flag = false;
				System.out.println(flag);
				return;
			}
		}
	}
}

```

# alternative

```cpp
#include <iostream>
using namespace std;

bool isPalindrome(string str, int size, int start, int end) {
	if(str[start]!=str[end])
		return false;
	else if(start==size/2)
		return true;
	else
		return isPalindrome(str, size, ++start, --end);
}

int main() {
	string s= "malayalam";
	int size= s.length();
	int start=0;
	int end= s.length()-1;
	isPalindrome(s, size, start, end)?cout<<"Palindrome":cout<<"Not Palindrome";
	
	return 0;
}
```

# 05 Anagram

        Write a function to check whether two given strings are anagram of each other or not. An anagram of a string is another string that contains same characters, only the order of characters can be different. For example, �abcd� and �dabc� are anagram of each other.


```java
package com.ignite.abdullah.array;

import java.util.Arrays;

public class _05Anagram {
	public static void main(String[] args) {
		String s1 = "AAB";
		String s2 = "ABA";

		char[] c1 = s1.toCharArray();
		char[] c2 = s2.toCharArray();

		Arrays.sort(c1);
		Arrays.sort(c2);
		boolean flag = false;
		if (c1.length == c2.length) {
			for (int i = 0; i < c2.length; i++) {
				if (c1[i] == c2[i])
					flag = true;
				else {
					flag = false;
					break;
				}
			}
		}
		System.out.println("Anagram status : " + flag);
	}
}

```

#  06 Count number of digits after `.` in floating point numbers?

        e.g. if given 3.554 output=3

        for 43.000 output=0.

```c++
// 06. Count number of digits after `.` in floating point numbers?
// e.g. if given 3.554 output=3

// for 43.000 output=0.

#include <iostream>
#include <cmath>
#include <math.h>
using namespace std;

bool is_integer(double k)
{
  return floor(k) == k;
}

// bool IsInt(double n)
// {
//     return !(n - trunc(n));
// }

int main() {
	double num;
	cin>>num;
	int counter=0;
	while(!is_integer(num)) {
		counter++;
		num= num*10;
	}
	cout<<"Number of digits after decimal point: "<<counter;
	return 0;
}
<!-- ================================================================================================= -->
```

# 07. program to count the occurrences of each character

        Write a program which prints number of occurrences of each characters and also it should not print repeatedly occurrences of duplicate characters as given in the example:

        Examples:

        Input : geeksforgeeks
        Output :
        Number of Occurrence of g is:2
        Number of Occurrence of e is:4
        Number of Occurrence of k is:2
        Number of Occurrence of s is:2
        Number of Occurrence of f is:1
        Number of Occurrence of o is:1
        Number of Occurrence of r is:1

```c++
// Write a program which prints number of occurrences of each characters and also it should not print repeatedly occurrences of duplicate characters as given in the example:

// Examples:

// Input : geeksforgeeks
// Output :
// Number of Occurrence of g is:2
// Number of Occurrence of e is:4
// Number of Occurrence of k is:2
// Number of Occurrence of s is:2
// Number of Occurrence of f is:1
// Number of Occurrence of o is:1
// Number of Occurrence of r is:1


#include <iostream>
#include <unordered_map>
#include <string>
using namespace std;

int main() {
	string str;
	cin>>str;
	unordered_map<char, int> omap;
	for(int i=0;i<str.length();i++) {
		omap[str[i]]++;
	}
	for(auto x : omap) {
		cout<<x.first<<": "<<x.second<<endl;
	}
	return 0;
}
<!--===================================================================================================================-->
```

# 08 Write program to find the years from a sentence

        Example:
        Bangladesh became independent in 1971 and 1948.
        Here 1971 and 1948 is the year.

```c++

// 08. Write program to find the years from a sentence
// Example:
// Bangladesh became independent in 1971 and 1948.
// Here 1971 and 1948 is the year.


#include <iostream>
#include <vector>
#define space ' '
using namespace std;

bool isdig(char c) {
	if(c>='0' && c<='9') 
		return true;
	return false;
}

bool isYear(string word) {
	if(word.length()!=4)
		return false;
	for(int i=0;i<word.length();i++) {
		if(!isdig(word[i]))
			return false;
	}
	return true;
}

vector<string> words(string sentence) {
	vector<string> words;
	string word="";
	cout<<"Years are: "<<endl;
	for(int i=0;i<=sentence.length();i++) {
		if(sentence[i]==space || i== sentence.length()) {
			words.push_back(word);
			word="";
		}
		else
			word+=sentence[i];
	}
	return words;
}

int main() {
	string sentence= "Bangladesh 1948 29 th became independent in 1971 and";
	for(auto word: words(sentence)) {
		if(isYear(word))
			cout<<word<<endl;
	}
	return 0;
}
```

# 09 LRU Cache

		Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.
		get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
		put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
		The cache is initialized with a positive capacity.



```java
package com.ignite.abdullah.array;

import java.util.LinkedHashMap;
import java.util.Map;

public class _09LRUCache {
	LinkedHashMap<Integer, Integer> map = new LinkedHashMap<> (); 
	@SuppressWarnings("serial")
	public _09LRUCache(int capacity) {
        map = new LinkedHashMap<>(capacity , 0.75f , true){
		
			//removing the eldest from map
            @SuppressWarnings("rawtypes")
			protected boolean removeEldestEntry(Map.Entry eldest){
              return map.size()>capacity;   
            }
        };
    }

	public int get(int key) {
		return map.getOrDefault(key, -1);
	}

	public void put(int key, int value) {
		map.put(key, value);
	}

	public static void main(String[] args) {
		_09LRUCache cache = new _09LRUCache( 2 /* capacity */ );

		cache.put(1, 1);
		cache.put(2, 2);
		cache.get(1);       // returns 1
		cache.put(3, 3);    // evicts key 2
		cache.get(2);       // returns -1 (not found)
		cache.put(4, 4);    // evicts key 1
		cache.get(1);       // returns -1 (not found)
		cache.get(3);       // returns 3
		System.out.println(cache.get(4));       // returns 4
	}
}

```



# 10 This challenge requires you to determine if a given number is a prime number which means it is only divisible by 1 and itself.

		prime number which means it is only divisible by 1 and itself. 
        The first few primes 
        are: 2, 3, 5, 7, 11, ... To solve this challenge, we'll create a loop that will run from the number 2 to N checking if N is evenly divisible by any number in the list. If N is divisible by some number it means that there is no remainder, so we'll be using the modulo function for this challenge.
        For example: if N = 9 we start a loop from 2 to 8
        9 / 2 gives a remainder of 1
        9 / 3 gives a remainder of 0 which means 9 is not a prime!

```c++
#include <iostream>
using namespace std;

bool isPrime(int num) {
	int flag=1;
	for(int i =2;i<=num/2;i++) {
		if(num%i==0) {
			flag=0;
			break;
		}
	}
	if(flag==1)
		return true;
	else
		return false;
}

int main() {
	int num;
	cin>>num;
	isPrime(num)?cout<<num<<" is prime" : cout<<num<<" is not prime";
	return 0;
}
```

# 11. Print All Distinct Elements of a given integer array

```cpp
#include <iostream>
#include<unordered_map>
using namespace std;

int main() {
	int arr[]={4,3,6,3,5,5,8,9,7,6};
	int size= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> elements;
	for(int i=0;i<size;i++) {
		elements[arr[i]]++;
	}
	for(auto x: elements) 
		cout<<x.first<<" ";
	return 0;
}
```

# 12. Reverse the words of a sentence

        Example: 
        Input: Bangladesh and Dhaka
        Output: Dhaka and Bangladesh

```cpp
#include <iostream>
#include<vector>
#define space ' '
using namespace std;

string reverseWords(string sentence) {
	vector<string> words;
	string word="";
	for(int i=0;i<=sentence.length();i++) {
		if(sentence[i]==space) {
			words.push_back(word);
			word="";
		}
		else if(i==sentence.length())
			words.push_back(word);
		else
			word+=sentence[i];
	}
	string result="";
	for(int i=0;i<words.size();i++) {
		if(i==words.size()-1)
			result=words[i]+result;
		else
			result=" "+words[i]+result;
	}
	
	return result;
}

int main() {
	cout<<reverseWords("Bangladesh and Dhaka");
	return 0;
}
```

# 13. Find the Missing Number

        You are given a list of n-1 integers and these integers are in the range of 1 to n. There are no duplicates in the list. One of the integers is missing in the list. Write an efficient code to find the missing integer.
        Example :
        Input: arr[] = {1, 2, 4,, 6, 3, 7, 8}
        Output: 5
        Input: arr[] = {1, 2, 3, 5}
        Output: 4

```cpp
#include <iostream>
#include<unordered_map>
using namespace std;

void swap(int *x, int* y) {
	int temp= *x;
	*x=*y;
	*y=temp;
}

void sort(int arr[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(arr[j]>arr[j+1])
				swap(&arr[j], &arr[j+1]);
		}
	}
}

int main() {
	int arr[] = {1, 2, 5, 6, 3, 7, 8};
	int size= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> umap;
	sort(arr,size);
	int max=arr[size-1];
	for(int i=0;i<size;i++) {
		umap[arr[i]]++;
	}
	for(int i=1;i<=max;i++) {
		if(umap[i]==0)
			cout<<i;
	}
	return 0;
}
```


# 13 FindTheMissingNumber
``` java
package com.ignite.abdullah.array;

public class _13FindTheMissingNumber {

	public static void main(String[] args) {
		var arr = new int[] { 1, 2, 4, 6, 3, 7, 8 };
		var l = arr.length;
		var formula = ((l+1)*(l+2))/2;
		
		for (var i = 0; i < l; i++) {
			formula -= arr[i];
		}
		
		System.out.println(formula);
	}
}
```

# 14. Have the function MaximalSquare(strArr)

 take the strArr parameter being passed which will be a 2D matrix of 0 and 1's, and determine the area of the largest square submatrix that contains all 1's. A square submatrix is one of equal width and height, and your program should return the area of the largest submatrix that contains only 1's. For example: if strArr is ["10100", "10111", "11111", "10010"] then this looks like the following matrix: 

        1 0 1 0 0
        1 0 1 1 1
        1 1 1 1 1
        1 0 0 1 0 

        For the input above, you can see the bolded 1's create the largest square submatrix of size 2x2, so your program should return the area which is 4. You can assume the input will not be empty. 
        Hard challenges are worth 15 points and you are not timed for them.
        Sample Test Cases
        Input:{"0111", "1111", "1111", "1111"}
        Output:9
        Input:{"0111", "1101", "0111"}
        Output:1

```cpp
#include<bits/stdc++.h>
#include <iostream>
#include<algorithm>
#include<string>
using namespace std;

int MaximalSquare(string strArr[], int inpArrSize) { 
	
	
	int eachStrSize= strArr[0].length();
	int mainMatrix[inpArrSize][eachStrSize];
    for(int i=0;i<inpArrSize;i++) {
        for(int j=0;j<eachStrSize;j++) {
            mainMatrix[i][j]=(strArr[i][j]-'0');
        }
    }

    int subMatrix[inpArrSize][eachStrSize];

    for(int i=0;i<eachStrSize;i++) {
        subMatrix[0][i]=mainMatrix[0][i];
    }

    for(int j=0;j<inpArrSize;j++) {
        subMatrix[j][0]=mainMatrix[j][0];
    }

    for(int i=1;i<inpArrSize;i++) {
        for(int j=1;j<eachStrSize;j++) {
            if(mainMatrix[i][j]==1)
                subMatrix[i][j]=min(subMatrix[i-1][j-1], min(subMatrix[i][j-1], subMatrix[i-1][j]))+1;
            else
                subMatrix[i][j]=0;
        }
    }
    int max=1;

    for(int i=1;i<inpArrSize;i++) {
        for(int j=1;j<eachStrSize;j++) {
            if(subMatrix[i][j]>max)
                max=subMatrix[i][j];
        }
    }
  
	return max*max; 
            
}

int main() {
	string strArr[]={
					"01101",  
                    "11010",  
                    "01110",  
                    "11110",  
                    "11111",  
                    "00000"
					};
	int inpArrSize= sizeof(strArr)/sizeof(strArr[0]);
    cout<<"Largest square submatrix of the given matrix: "<<MaximalSquare(strArr,inpArrSize);
	return 0;
}
```

# 15. Have the function PentagonalNumber(num)

read num which will be a positive integer and determine how many dots exist in a pentagonal shape around a center dot on the Nth iteration. For example, in the image below you can see that on the first iteration there is only a single dot, on the second iteration there are 6 dots, on the third there are 16 dots, and on the fourth there are 31 dots. 

        Your program should return the number of dots that exist in the whole pentagon on the Nth iteration. 
        Hard challenges are worth 15 points and you are not timed for them.
        Sample Test Cases
        Input:2
        Output:6
        Input:5
        Output:51

```cpp
#include <iostream>
#include <string>
using namespace std;

int PentagonalNumber(int num) { 

  return 1 + 5*(num * (num - 1) / 2);
            
}

int main() { 
  
  cout << PentagonalNumber(5);
  return 0;
    
} 

```
# 16.  Have the function EightQueens(strArr) read strArr which will be an array consisting of the locations of eight Queens on a standard 8x8 chess board with no other pieces on the board. The structure of strArr will be the following: ["(x,y)", "(x,y)", ...] where (x,y) represents the position of the current queen on the chessboard (x and y will both range from 1 to 8 where 1,1 is the bottom-left of the chessboard and 8,8 is the top-right). Your program should determine if all of the queens are placed in such a way where none of them are attacking each other. If this is true for the given input, return the string true otherwise return the first queen in the list that is attacking another piece in the same format it was provided. 

        For example: if strArr is ["(2,1)", "(4,2)", "(6,3)", "(8,4)", "(3,5)", "(1,6)", "(7,7)", "(5,8)"] then your program should return the string true. The corresponding chessboard of queens for this input is below (taken from Wikipedia). 
        
        Sample Test Cases
        Input:{"(2,1)", "(4,3)", "(6,3)", "(8,4)", "(3,4)", "(1,6)", "(7,7)", "(5,8)"}
        Output:"(2,1)"
        Input:{"(2,1)", "(5,3)", "(6,3)", "(8,4)", "(3,4)", "(1,8)", "(7,7)", "(5,8)"}
        Output:"(5,3)"




# 17. Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string.

If there are two or more words that are the same length, return the first word from the string with that length. Ignore punctuation and assume sen will not be empty. 

        Sample Test Cases
        Input:"fun&!! time"
        Output:"time"
        Input:"I love dogs"
        Output:"love"

```java
package com.ignite.abdullah.array;

public class _17LongestWord {
	public static void main(String[] args) {

		var str = "I love dogs";
		
		var arr = str.split(" ");
		var res = "";
		for (var i : arr) {
			if (res.length() < i.length())
				res = i;
		}
		System.out.println(res);
	}
}

```



# 18. Have the function ClosestEnemyII(strArr) read the matrix of numbers stored in strArr which will be a 2D matrix that contains only the integers 1, 0, or 2. Then from the position in the matrix where a 1 is, return the number of spaces either left, right, down, or up you must move to reach an enemy which is represented by a 2. You are able to wrap around one side of the matrix to the other as well. For example: if strArr is ["0000", "1000", "0002", "0002"] then this looks like the following: 

        0 0 0 0
        1 0 0 0
        0 0 0 2
        0 0 0 2 

        For this input your program should return 2 because the closest enemy (2) is 2 spaces away from the 1 by moving left to wrap to the other side and then moving down once. The array will contain any number of 0's and 2's, but only a single 1. It may not contain any 2's at all as well, where in that case your program should return a 0. 
        Sample Test Cases
        Input:{"000", "100", "200"}
        Output:1
        Input:{"0000", "2010", "0000", "2002"}
        Output:2

```java
package com.ignite.abdullah.array;

import java.util.ArrayList;

public class _18ClosestEnemyII {
	public static String ClosestEnemyII(String[] strArr) {

		int[] start = new int[] { 0, 0 };
		int closest = 0;
		int x;
		int y;
		int temp0, temp1; // for clarity
		ArrayList<int[]> enemies = new ArrayList<>();
		char[][] matrix = new char[strArr.length][];
		for (int i = 0; i < strArr.length; i++) {
			matrix[i] = strArr[i].toCharArray();
		}
		
		
		for (char[] cs : matrix) {
			for (char cs2 : cs) {
				System.out.print(cs2 + " ");
			}
			System.out.println();
		}
		
		
		for (int i = 0; i < matrix.length; i++) { // enumeration
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == '1') {
					start = new int[] { i, j };
				}
				if (matrix[i][j] == '2') {
					enemies.add(new int[] { i, j });
				}
			}
		}
		for (int[] e : enemies) {
			temp0 = Math.abs(strArr[0].length() - start[0] + e[0]);
			temp1 = Math.abs(strArr[0].length() - e[0] + start[0]);
			x = Math.min(Math.abs(start[0] - e[0]), Math.min(temp0, temp1));
			temp0 = Math.abs(strArr.length - start[1] + e[1]);
			temp1 = Math.abs(strArr.length - e[1] + start[1]);
			y = Math.min(Math.abs(start[1] - e[1]), Math.min(temp0, temp1));

			if (closest == 0 || closest > (x + y)) {
				closest = x + y;
			}
		}
		return String.valueOf(closest);
	}

	public static void main(String[] args) {
		System.out.print(ClosestEnemyII(new String[] { "000", "100", "200" }));
	}
}

```

# 19 MedianOfTwoSortedArrays

Given two sorted arrays, a[] and b[], task is to find the median of these sorted arrays, in O(log(min(n, m)), when n is the number of elements in the first array, and m is the number of elements in the second array.

	Input : ar1[] = {-5, 3, 6, 12, 15}
	        ar2[] = {-12, -10, -6, -3, 4, 10}
	        The merged array is :
	        ar3[] = {-12, -10, -6, -5 , -3,
	                 3, 4, 6, 10, 12, 15}
	Output : The median is 3.

	Input : ar1[] = {2, 3, 5, 8}
	        ar2[] = {10, 12, 14, 16, 18, 20}
	        The merged array is :
	        ar3[] = {2, 3, 5, 8, 10, 12, 14, 16, 18, 20}
	        if the number of the elements are even, 
	        so there are two middle elements,
	        take the average between the two :
	        (10 + 12) / 2 = 11.      
	Output : The median is 11.


```java
package com.ignite.abdullah.array;

public class _19MedianOfTwoSortedArrays {

	static int maximum(int a, int b) {
		return a > b ? a : b;
	}

	static int minimum(int a, int b) {
		return a < b ? a : b;
	}

	static double findMedianSortedArrays(int[] arr1, int[] arr2) {

		int n = arr1.length, m = arr2.length;
		int min = 0, max = n;
		int i = 0, j = 0, median = 0;

		while (min <= max) {
			i = (min + max) / 2;
			j = ((n + m + 1) / 2) - i;
			if (i < n && j > 0 && arr2[j - 1] > arr1[i])
				min = i + 1;
			else if (i > 0 && j < m && arr2[j] < arr1[i - 1])
				max = i - 1;
			else {
				if (i == 0)
					median = arr2[j - 1];
				else if (j == 0)
					median = arr1[i - 1];
				else
					median = maximum(arr1[i - 1], arr2[j - 1]);
				break;
			}
		}

		if ((n + m) % 2 == 1)
			return (double) median;

		if (i == n)
			return (median + arr2[j]) / 2.0;

		if (j == m)
			return (median + arr1[i]) / 2.0;

		return (median + minimum(arr1[i], arr2[j])) / 2.0;
	}

	public static void main(String[] args) {
		int[] arr1 = new int[] { 900 };
		int[] arr2 = new int[] { 10, 13, 14 };
		System.out.print("The median is : " + findMedianSortedArrays(arr1, arr2));
	}
}
```

# 20 Remove Duplicates From Sorted Array

        Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.

        Input  : arr[] = {2, 2, 2, 2, 2}
        Output : arr[] = {2}
        new size = 1
        Input  : arr[] = {1, 2, 2, 3, 4, 4, 4, 5, 5}
        Output : arr[] = {1, 2, 3, 4, 5}
        new size = 5

```java
package com.ignite.abdullah.array;

public class _20RemoveDuplicatesFromSortedArray {
	private static int removeDuplicate(int[] arr) {

		int[] temp = new int[arr.length];
		int i = 0;
		for (int j = 0; j < arr.length - 1; j++) {
			temp[i] = arr[j];
			i++;
			while ((arr[j] == arr[j + 1]) && j < arr.length - 2) {
				j++;
			}
		}
		
		if(arr[arr.length-2] != arr[arr.length-1]) 
			temp[i++] = arr[arr.length-1];
		
		for (int j = 0; j < i; j++) {
			arr[j] = temp[j];
		}
		return i;
	}

	public static void main(String[] args) {
		int[] arr = { 1, 2, 2, 3, 4, 4, 4, 5, 5,6 };

		int newLength = removeDuplicate(arr);

		for (int i = 0; i < newLength; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}
```

# 21. Rotate Array

        Given an array, rotate the array to the right by k steps, where k is non-negative.
        Example 1:
        Input: [1,2,3,4,5,6,7] and k = 3
        Output: [5,6,7,1,2,3,4]
        Explanation:
        rotate 1 steps to the right: [7,1,2,3,4,5,6]
        rotate 2 steps to the right: [6,7,1,2,3,4,5]
        rotate 3 steps to the right: [5,6,7,1,2,3,4]
        Example 2:
        Input: [-1,-100,3,99] and k = 2
        Output: [3,99,-1,-100]
        Explanation: 
        rotate 1 steps to the right: [99,-1,-100,3]
        rotate 2 steps to the right: [3,99,-1,-100]

```cpp
#include <iostream>
using namespace std;

void rotateKTimes(int arr[], int n, int k) {
	for(int i=0;i<k;i++) {
		int temp= arr[n-1];
		for(int i=n-1;i>0;i--) {
			arr[i]=arr[i-1];
		}
		arr[0]=temp;
	}
}

int main() {
	int arr[]= {-1,-100,3,99};
	int size= sizeof(arr)/sizeof(arr[0]);
	rotateKTimes(arr,size,2);
	
	for(int i=0;i<size;i++) {
		cout<<arr[i]<<" ";
	}
	return 0;
}
```

# 22. Valid Parentheses

        Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
        An input string is valid if:
        Open brackets must be closed by the same type of brackets.
        Open brackets must be closed in the correct order.
        Note that an empty string is also considered valid.
        Example 1:
        Input: "()"
        Output: true
        Example 2:
        Input: "()[]{}"
        Output: true
        Example 3:
        Input: "(]"
        Output: false
        Example 4:
        Input: "([)]"
        Output: false
        Example 5:
        Input: "{[]}"
        Output: true

```cpp
#include <iostream>
#include<bits/stdc++.h>
#include<unordered_map>
#define SIZE 101
using namespace std;

int top=-1;
char Stk[SIZE];

void push(char c) {
    if(top==SIZE-1) {
        cout<<"Overflow";
        return;
    }
    Stk[++top]=c;
}

void pop() {
    if(top==-1) {
        cout<<"Underflow";
        return;
    }
    top--;
}

bool isValid(string Parentheses) {
    string starts="[{(";
    string ends= "]})";
    unordered_map<char,char> umap;
    for(int i=0;i<starts.length();i++) {
        umap[starts[i]]=ends[i];
    }
    for(int i=0;i<Parentheses.length();i++) {
        for(auto x: umap) {
            if(Parentheses[i]==x.first) 
                push(Parentheses[i]);
            else if(Parentheses[i]==x.second && x.first==Stk[top])
                pop();
            else if(top==-1 && Parentheses[i]==x.second){
            	push(Parentheses[i]);
            	break;
            }
            	
        }
    }
    if(top==-1)
        return true;
    else
        return false;
}


int main() {
    isValid("[[[[[[[[[{{{{{{{(((Naimul)))}}}}}}}]]]]]]]]]")?cout<<"true":cout<<"false";
	return 0;
}
```

# 23. Find the minimum number of platforms required for railway station

		You are given arrival and departure time of trains reaching to a particular station. You need to find the minimum number of platforms required to accommodate the trains at any point in time.
        For example:
        arrival[] = {1:00, 1:40, 1:50, 2:00, 2:15, 4:00} 
        departure[] = {1:10, 3:00, 2:20, 2:30, 3:15, 6:00}
        No. of platforms required in above scenario = 4
        Please note that arrival time is in chronological order.

```cpp
#include <iostream>
#include<vector>
using namespace std;

string split(string s) {
	string split="";
	for(int i=0;i<s.length();i++) {
		if(s[i]!=':') 
			split+=s[i];
	}
	return split;
}

int minPlatforms(string arriv[], string depart[], int size) {
	vector<int> arrival;
    vector<int> departure;
    for(int i=0;i<size;i++) {
    	arrival.push_back(stoi(split(arriv[i])));
    	departure.push_back(stoi(split(depart[i])));
    }
    int platform=1;
    for(int i=1;i<size;i++) {
    	int temp=platform;
    	int flag=0;
    	while(temp>0) {
    		if(arrival[i]<departure[i-temp])
    			flag=1;
    		else {
    			flag=0;
    			break;
    		}
    		temp--;
    	}
    	if(flag==1)
    		platform++;
    }
    return platform;
}

int main() {
	string arrival[] = {"9:00", "9:40", "9:50", "11:00", "15:00", "18:00"};
    string departure[] = {"9:10", "12:00", "11:20", "11:30", "19:00", "20:00"};
    int size= sizeof(arrival)/sizeof(arrival[0]);
    cout<<minPlatforms(arrival, departure, size);
	return 0;
}
```

# 24. Find a Pair Whose Sum is Closest to zero in Array

        Given an array of +ve and -ve integers, we need to find a pair whose sum is closed to Zero in Array.
        For example:
        array[]={1,3,-5,7,8,20,-40,6};
        The pair whose sum is closest to zero:  -5 and 6

```cpp
#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int main() {
	int array[]={1,3,-5,7,8,20,-40,6};
	int size= sizeof(array)/sizeof(array[0]);
	int min= INT_MAX;
	int pair[2];
	for(int i=0;i<size;i++) {
		for(int j=0;j<size;j++) {
			int sum= array[i]+array[j];
			int diff= abs(sum);
			if(diff<min) {
				min=diff;
				pair[0]=array[i];
				pair[1]= array[j];
			}
				
		}
	}
	cout<<pair[0]<<" and "<<pair[1];
	
	return 0;
}
```

#  25. Find all pairs of elements from an array whose sum is equal to given number

        Given an array, we need to find all pairs whose sum is equal to number X.
        For example:
        array[]={ -40, -5, 1, 3, 6, 7, 8, 20 };
        Pair of elements whose sum is equal to 15:  7, 8 and -5, 20


```cpp
#include <iostream>
#include<bits/stdc++.h>
#include<map>
using namespace std;

int main() {
	int array[]={-40, -5, 1, 3, 6, 7, 8, 20};
	int x= 15;
	int size= sizeof(array)/sizeof(array[0]);
	map<int,int> mymap;
	int pair[2];
	for(int i=0;i<size-1;i++) {
		for(int j=i+1;j<size;j++) {
			int sum= array[i]+array[j];
			if(sum==x) {
				pair[0]=array[i];
				pair[1]= array[j];
				mymap.insert({pair[0],pair[1]});
			}
				
		}
	}
	for(auto x: mymap)
		cout<<x.first<<", "<<x.second<<endl;
	
	return 0;
}
```

# 26 Given an array of 0’s and 1’s in random order, you need to separate 0’s and 1’s in an array.

        For example:
        arr[] = {0,1,0,0,1,1,1,0,1}
        Array after separating 0 and 1 numbers:
        {0,0,0,0,1,1,1,1,1}

```cpp
#include <iostream>
#include<vector>
using namespace std;

int main() {
	int arr[] = {0,1,0,0,1,1,1,0,1};
	int size= sizeof(arr)/sizeof(arr[0]);
	
	int zeroCounter=0;
	for(int i=0;i<size;i++) {
		if(arr[i]==0)
			zeroCounter++;
	}
	
	for(int j=0;j<zeroCounter;j++) {
		arr[j]=0;
	}
	for(int k=zeroCounter;k<size;k++) {
		arr[k]= 1;
	}
	for(int i=0;i<size;i++) {
		cout<<arr[i]<<" ";
	}
	
	return 0;
}
```

# 27. Separate odd and even numbers in an array

        Given an array of integers, you need to segregate odd and even numbers in an array.
        Please note: Order of elements can be changed.
        For example:
        arr[] = {12, 17, 70, 15, 22, 65, 21, 90}
        Array after separating odd and even numbers:
        {12, 90, 70, 22, 15, 65, 21, 17}

```cpp
#include <iostream>
using namespace std;
 
void swap(int *x, int *y) {
	int temp= *x;
	*x=*y;
	*y=temp;
}
 
int main() {
	int arr[] = {12, 17, 70, 15, 22, 65, 21, 90};
	int size= sizeof(arr)/sizeof(arr[0]);
	int j=-1;
	for(int i=0;i<size;i++) {
		if(arr[i]%2==0) {
			j++;
			swap(&arr[i], &arr[j]);
		}
	}
	for(int j=0;j<size;j++) {
		cout<<arr[j]<<" ";
	}
	return 0;
}
```

# 28. Given an array containing zeroes, ones and twos only. Write a function to sort the given array in O(n) time complexity.

        For example:
        Input:
        [1, 2, 2, 0, 0, 1, 2, 2, 1]
        
        Output:
        [0, 0, 1, 1, 1, 2, 2, 2, 2]

```cpp
#include <iostream>
using namespace std;

int main() {
	int arr[]={1, 2, 2, 0, 0, 1, 2, 2, 1};
	int size= sizeof(arr)/sizeof(arr[0]);
	int count0, count1, count2;
	count0=0;
	count1=0;
	count2=0;
	for(int i=0;i<size;i++) {
		if(arr[i]==0)
			count0++;
		if(arr[i]==1)
			count1++;
		if(arr[i]==2)
			count2++;
	}
	for(int i=0;i<count0;i++) {
		arr[i]=0;
	}
	for(int i=count0;i<(count0+count1);i++) {
		arr[i]=1;
	}
	for(int i=(count0+count1);i<size;i++) {
		arr[i]=2;
	}
	for(int j=0;j<size;j++) {
		cout<<arr[j]<<" ";
	}
	return 0;
}
```

# 29. Find local minima in array

        A local minima is less than its neighbours
        For example:
        Input:
        int [] arr = {10, 5, 3, 6, 13, 16, 7};
        Output: 3
        
        int []arr = {11,12,13,14};
        Output: 11
        
        int []arr = {10};
        Output: 10
        
        int []arr = {8,6};
        Output: 6

```cpp
#include <iostream>
using namespace std;
// int [] arr = {10, 5, 3, 6, 13, 16, 7};
int localminima(int arr[],int n) {
	if(n==1) 
		return arr[0];
	for(int i=0;i<n;i++) {
		if(i==0 && arr[i]<arr[i+1])
			return arr[i];
		if(i==n-1 && arr[i]<arr[i-1])
			return arr[i];
		else {
			if(arr[i]<arr[i-1] && arr[i]<arr[i+1])
				return arr[i];
		}
	}
}

int main() {
	int arr[] = {8,6};
	int size= sizeof(arr)/sizeof(arr[0]);
	cout<<localminima(arr,size);
	return 0;
}
```

# 30. Sliding window maximum in java

        Given an Array of integers and an Integer k, Find the maximum element of from all the contiguous subarrays of size K.
        For example:
        Input:
        Input: int[] arr = {2,6,-1,2,4,1,-6,5}
        int k = 3
        output: 6,6,4,4,4,5


```cpp
#include <iostream>
#include<vector>
#include<bits/stdc++.h>
using namespace std;



int main() {
	int arr[] = {2,6,-1,2,4,1,-6,5};
	int k=3;
	int n= sizeof(arr)/sizeof(arr[0]);
	vector<int> vec;
	for(int i=0;i<=n-k;i++) {
		int max= INT_MIN;
		for(int j=0;j<k;j++) {
			if(arr[i+j]>max)
				max= arr[i+j];
		}
		vec.push_back(max);
	}
	for(auto x: vec) 
		cout<<x<<" ";
	return 0;
}
```

# 33. find all elements in array which have at-least two greater elements

	Given an array of n distinct elements, the task is to find all elements in array which have at-least two greater elements than themselves.
	Examples :
	Input : arr[] = {2, 8, 7, 1, 5};
	Output : 2  1  5  
	The output three elements have two or
	more greater elements
	      
	Input  : arr[] = {7, -2, 3, 4, 9, -1};
	Output : -2  3  4 -1  

```cpp
#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;

void swap(int *x, int *y) {
	int temp= *x;
	*x= *y;
	*y= temp;
}

void sort(int a[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(a[j]>a[j+1])
				swap(&a[j], &a[j+1]);
		}
	}
}

int main() {
	int arr[]= {7, 12, 9, 15, 19, 32, 56, 70};
	int n= sizeof(arr)/sizeof(arr[0]);
	sort(arr,n);
	for(int i=0;i<n-2;i++) {
		cout<<arr[i]<<" ";
	}
	
	return 0;
}
```

# 34. find k pairs with smallest sums in two arrays

	Given two integer arrays arr1[] and arr2[] sorted in ascending order and an integer k. Find k pairs with smallest sums such that one element of a pair belongs to arr1[] and other element belongs to arr2[]

	Examples:
	Input :  arr1[] = {1, 7, 11}
	         arr2[] = {2, 4, 6}
	         k = 3
	Output : [1, 2],
	         [1, 4],
	         [1, 6]
	Explanation: The first 3 pairs are returned 
	from the sequence [1, 2], [1, 4], [1, 6], 
	[7, 2], [7, 4], [11, 2], [7, 6], [11, 4], 
	[11, 6]



```cpp
#include <iostream>
#include<bits/stdc++.h>
#include<vector>
#include<map>
using namespace std;

int main() {
	int nums1[]= {1,2};
	int nums2[]= {3};
	int k=3;
	int n1= sizeof(nums1)/sizeof(nums1[0]);
	int n2= sizeof(nums2)/sizeof(nums2[0]);
	map<int, vector<int>> mymap;
	vector<int> myvec;
	for(int i=0;i<n1;i++) {
		for(int j=0;j<n2;j++) {
			vector<int> temp;
			int sum= nums1[i]+nums2[j];
			temp.push_back(nums1[i]);
			temp.push_back(nums2[j]);
			mymap.insert({sum, temp});
		}
	}
	
	for(auto x: mymap)
		myvec.push_back(x.first);
		
	sort(myvec.begin(),myvec.end());
	map<int,int> tempo;
	for(int i=0;i<k;i++) {
		cout<<mymap[myvec[i]][0]<<", "<<mymap[myvec[i]][1]<<endl;
		
	}
	
	return 0;
}
```

# 35. find the smallest and second smallest elements in an array

	Find the smallest and second smallest elements in an array
	Input:  arr[] = {12, 13, 1, 10, 34, 1}
	Output: The smallest element is 1 and 
	        second Smallest element is 10

```cpp
#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;

void swap(int *x, int *y) {
	int temp= *x;
	*x= *y;
	*y= temp;
}

void sort(int a[], int n) {
	for(int i=0;i<n-1;i++) {
		for(int j=0;j<n-i-1;j++) {
			if(a[j]>a[j+1])
				swap(&a[j], &a[j+1]);
		}
	}
}

int main() {
	int arr[]= {2,6,-1,2,4,1,-6,5};
	int n= sizeof(arr)/sizeof(arr[0]);
	sort(arr,n);
	cout<<arr[0]<<" "<<arr[1];
	
	return 0;
}
```

# 36. find the smallest missing number

	Given a sorted array of n distinct integers where each integer is in the range from 0 to m-1 and m > n. Find the smallest number that is missing from the array.

	Examples
	Input: {0, 1, 2, 6, 9}, n = 5, m = 10
	Output: 3
	Input: {4, 5, 10, 11}, n = 4, m = 12
	Output: 0
	Input: {0, 1, 2, 3}, n = 4, m = 5
	Output: 4
	Input: {0, 1, 2, 3, 4, 5, 6, 7, 10}, n = 9, m = 11
	Output: 8


```cpp
#include <iostream>
#include<bits/stdc++.h>
#include<unordered_map>
using namespace std;

int main() {
	int arr[]= {1,2,3,4,6,9,11,15};
	int n= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> mmap;
	for(int i=0;i<n;i++) {
		mmap[arr[i]]++;
	}
	for(int i=0;i<n;i++) {
		if(mmap[i]==0) {
			cout<<i;
			break;
		}
		
	}
	
	
	return 0;
}
```


```java
package com.ignite.abdullah.array;

public class _36FindTheSmallestMissingNumber {

	static int findFirstMissing(int array[], int start, int end) {
		if (start > end)
			return end + 1;
		if (start != array[start])
			return start;
		int mid = (start + end) / 2;
		if (array[mid] == mid)
			return findFirstMissing(array, mid + 1, end);
		return findFirstMissing(array, start, mid);
	}

	public static void main(String[] args) {
		int arr[] = { 0, 1, 2, 3, 4, 5, 6, 7, 10 };
		int n = arr.length;
		System.out.println("First Missing element is : " + findFirstMissing(arr, 0, n - 1));
	}
}

```


# 37. find the minimum distance between tow numbers


	Given an unsorted array arr[] and two numbers x and y, find the minimum distance between x and y in arr[]. The array might also contain duplicates. You may assume that both x and y are different and present in arr[].

	Examples:
	Input: arr[] = {1, 2}, x = 1, y = 2
	Output: Minimum distance between 1 and 2 is 1.
	Input: arr[] = {3, 4, 5}, x = 3, y = 5
	Output: Minimum distance between 3 and 5 is 2.
	Input: arr[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3}, x = 3, y = 6
	Output: Minimum distance between 3 and 6 is 4.
	Input: arr[] = {2, 5, 3, 5, 4, 4, 2, 3}, x = 3, y = 2
	Output: Minimum distance between 3 and 2 is 1.


```cpp
#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int distance(int arr[], int n, int x, int y) {
	int minDistance=INT_MAX;
	for(int i=0;i<n-1;i++) {
		for(int j=i+1;j<n;j++) {
			if((x==arr[i] && y==arr[j]) || (y==arr[i] && x==arr[j]) && abs(i-j)<minDistance)
				minDistance= abs(i-j);
		}
	}
	return minDistance;
}

int main() {
	
	int arr[] = {3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3};
	int size= sizeof(arr)/sizeof(arr[0]);
	int x=3;
	int y=6;
	cout<<distance(arr,size,x,y);
	
	return 0;
}
```

```java
package com.ignite.abdullah.array;

public class _37FindTheMinimumDistanceBetweenTwoNumbers {

	static int minDist(int arr[], int n, int x, int y) {
		int i, j;
		int min_dist = Integer.MAX_VALUE;
		for (i = 0; i < n; i++) {
			for (j = i + 1; j < n; j++) {
				if ((x == arr[i] && y == arr[j] || y == arr[i] && x == arr[j]) && min_dist > Math.abs(i - j))
					min_dist = Math.abs(i - j);
			}
		}
		return min_dist;
	}

	public static void main(String[] args) {
		int arr[] = { 3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3 };
		int n = arr.length;
		int x = 3;
		int y = 6;

		System.out.println("Minimum distance between " + x + " and " + y + " is " + minDist(arr, n, x, y));
	}

}

```

# 38. find the smallest positive number missing from an unsorted array

	You are given an unsorted array with both positive and negative elements. You have to find the smallest positive number missing from the array in O(n) time using constant extra space. You can modify the original array.
	Examples

	 Input:  {2, 3, 7, 6, 8, -1, -10, 15}
	 Output: 1
	 Input:  { 2, 3, -7, 6, 8, 1, -10, 15 }
	 Output: 4
	 Input: {1, 1, 0, -1, -2}
	 Output: 2 

```cpp
#include <iostream>
#include<bits/stdc++.h>
#include<unordered_map>
using namespace std;

int main() {
	int arr[]= {1,2,3,4,6,9,11,15};
	int n= sizeof(arr)/sizeof(arr[0]);
	unordered_map<int,int> mmap;
	for(int i=0;i<n;i++) {
		mmap[arr[i]]++;
	}
	for(int i=0;i<n;i++) {
		if(mmap[i]==0) {
			cout<<i;
			break;
		}
		
	}
	
	
	return 0;
}
```


# 39. FindTheClosestPairFromTwoSortedArrays

		Input:  ar1[] = {1, 4, 5, 7};
		        ar2[] = {10, 20, 30, 40};
		        x = 32      
		Output:  1 and 30
		Input:  ar1[] = {1, 4, 5, 7};
		        ar2[] = {10, 20, 30, 40};
		        x = 50      
		Output:  7 and 40

```java
package com.ignite.abdullah.array;

public class _39FindTheClosestPairFromTwoSortedArrays {

	static void closest(int ar1[], int ar2[], int m, int n, int x) {
		int diff = Integer.MAX_VALUE;

		int res_l = 0, res_r = 0;

		int l = 0, r = n - 1;
		while (l < m && r >= 0) {
			if (Math.abs(ar1[l] + ar2[r] - x) < diff) {
				res_l = l;
				res_r = r;
				diff = Math.abs(ar1[l] + ar2[r] - x);
			}

			if (ar1[l] + ar2[r] > x)
				r--;
			else
				l++;
		}

		System.out.print("The closest pair is [" + ar1[res_l] + ", " + ar2[res_r] + "]");
	}

	// Driver program to test above functions
	public static void main(String args[]) {
		int ar1[] = { 1, 4, 5, 7 };
		int ar2[] = { 10, 20, 30, 40 };
		int m = ar1.length;
		int n = ar2.length;
		int x = 38;
		closest(ar1, ar2, m, n, x);
	}

}

```

# 40. find the largest pair sum in an unsorted array
```cpp
#include <iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;
 
 
int main() {
	int arr[]= {12, 34, 10, 6, 40};
	int n= sizeof(arr)/sizeof(arr[0]);
	int pair[2];
	int largestSum=INT_MIN;
	for(int i=0;i<n-1;i++) {
		for(int j=i+1;j<n;j++) {
			int sum= arr[i]+arr[j];
			if(sum>largestSum) {
				largestSum=sum;
				pair[0]= arr[i];
				pair[1]= arr[j];
			}
 
		}
	}
	cout<<"Largest sum: "<<largestSum<<endl;
	cout<<"Pair: "<<pair[0]<<", "<<pair[1];
 
	return 0;
}
```

41. print the pattern using only a single loop: 98765432123456789

```cpp
#include <iostream>
using namespace std;

void PrintPattern(int m) 
{ 
    if (m > 1) 
    { 
        cout << m << " "; 
        PrintPattern(m - 1); 
    } 
  
    cout << m << " "; 
    m += 1; 
} 
  
int main() 
{ 
     int n = 9; 
     PrintPattern(n); 
    return 0; 
} 
```


# 
```java

```


# 
```java

```


# 
```java

```


# 
```java

```


# 
```java

```


# 
```java

```
